package com.lizc.mapper;

import com.lizc.model.ProductDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizengcai
 * @since 2024-04-05
 */
public interface ProductMapper extends BaseMapper<ProductDO> {

}
