package com.lizc.aspect;

import com.lizc.annotation.RepeatSubmit;
import com.lizc.constant.RedisKey;
import com.lizc.enums.BizCodeEnum;
import com.lizc.exception.BizException;
import com.lizc.interceptor.LoginInterceptor;
import com.lizc.model.LoginUser;
import com.lizc.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * 定义一个切面类
 */
@Slf4j
@Component
@Aspect
public class RepeatSubmitAspect {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    /**
     * 定义 @Pointcut注解表达式，在哪里执行
     *  方式一：@annotation：当执行的方法上拥有指定的注解时生效（我们采用这个）
     *      RepeatSubmit 入参表示这个注解的实现类，也就是具体的注解使用
     *  方式二：execution：  一般用于指定方法的执行
     *
     * @param repeatSubmit
     */
    @Pointcut("@annotation(repeatSubmit)")
    public void pointcutNoRepeatSubmit(RepeatSubmit repeatSubmit) {

    }


    /**
     * 环绕通知, 围绕着方法执行
     * @Around 可以用来在调用一个具体方法前和调用后来完成一些具体的任务。
     *
     * 方式一：单用 @Around("execution(* net.xdclass.controller.*.*(..))")可以
     * 方式二：用@Pointcut和@Around联合注解也可以（我们采用这个）  @Around里面写@Pointcut对应方法的调用
     *
     * 两种方式
     * 方式一：加锁 固定时间内不能重复提交
     * <p>
     * 方式二：先请求获取token，这边再删除token,删除成功则是第一次提交
     *
     * @param joinPoint
     * @param repeatSubmit
     * @return
     * @throws Throwable
     */
    @Around("pointcutNoRepeatSubmit(repeatSubmit)")
    public Object around(ProceedingJoinPoint joinPoint, RepeatSubmit repeatSubmit) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        //用于记录成功还是失败，成功则执行方法，失败则抛出异常
        boolean res = false;

        String type = repeatSubmit.limitType().name();
        if (type.equals(RepeatSubmit.Type.PARAM.name())) {
            //方式一方法参数            TODO
            long lockTime = repeatSubmit.lockTime();
            String ipAddr = CommonUtil.getIpAddr(request);
            //获取注解
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            Method method = signature.getMethod();
            //目标类、方法
            String className = method.getDeclaringClass().getName();
            String name = method.getName();
            //使用MD5减少key的长度
            String cacheKey = "order-server:repeat_submit:" + CommonUtil.MD5(String.format("%s-%s-%s-%s",ipAddr, className, name, loginUser.getAccountNo()));
            log.info("key={}", cacheKey);

            //java代码里面   加锁，设置锁的超时时间 lockTime
            //res = stringRedisTemplate.opsForValue().setIfAbsent(cacheKey,"1", lockTime, TimeUnit.SECONDS);

            // 分布式锁
            RLock lock = redissonClient.getLock(cacheKey);

            // 尝试加锁，最多等待0秒(不等待直接加锁，失败直接返回)，上锁以后5秒自动解锁 [lockTime默认为5s, 可以自定义]
            res = lock.tryLock(0, lockTime, TimeUnit.SECONDS);
        } else {
            //方式二,令牌形式
            String requestToken = request.getHeader("request-token");
            if (StringUtils.isBlank(requestToken)) {
                throw new BizException(BizCodeEnum.ORDER_CONFIRM_TOKEN_EQUAL_FAIL);
            }

            //"order:submit:%s:%s"
            String key = String.format(RedisKey.SUBMIT_ORDER_TOKEN_KEY, loginUser.getAccountNo(),requestToken);
            /**
             * 提交表单的token key
             * 方式一：不用lua脚本获取再判断，之前是因为 key组成是 order:submit:accountNo, value是对应的token，所以需要先获取值，再判断
             * 方式二：可以直接key是 order:submit:accountNo:token,然后直接删除成功则完成
             */
            res = stringRedisTemplate.delete(key);
        }

        if (!res) {
            //如果不抛出异常，则打印日志, 并直接返回
            log.error("请求重复提交");
            return null;
            //throw new BizException(BizCodeEnum.ORDER_CONFIRM_REPEAT);
        }

        log.info("目标方法执行前");
        Object obj = joinPoint.proceed();
        log.info("目标方法执行后");
        return obj;
    }

}
