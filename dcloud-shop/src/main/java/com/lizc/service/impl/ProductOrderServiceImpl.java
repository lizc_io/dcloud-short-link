package com.lizc.service.impl;

import com.lizc.component.PayFactory;
import com.lizc.config.RabbitMQConfig;
import com.lizc.constant.TimeConstant;
import com.lizc.controller.request.ConfirmOrderRequest;
import com.lizc.controller.request.ProductOrderPageRequest;
import com.lizc.enums.*;
import com.lizc.exception.BizException;
import com.lizc.interceptor.LoginInterceptor;
import com.lizc.manager.ProductManager;
import com.lizc.manager.ProductOrderManager;
import com.lizc.model.EventMessage;
import com.lizc.model.LoginUser;
import com.lizc.model.ProductDO;
import com.lizc.model.ProductOrderDO;
import com.lizc.service.ProductOrderService;
import com.lizc.util.CommonUtil;
import com.lizc.util.JsonData;
import com.lizc.util.JsonUtil;
import com.lizc.vo.PayInfoVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizengcai
 * @since 2024-04-05
 */
@Slf4j
@Service
public class ProductOrderServiceImpl implements ProductOrderService {

    @Autowired
    private ProductOrderManager productOrderManager;

    @Autowired
    private ProductManager productManager;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitMQConfig rabbitMQConfig;

    @Autowired
    private PayFactory payFactory;

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;

    @Override
    public Map<String, Object> page(ProductOrderPageRequest orderPageRequest) {
        Long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();
        return productOrderManager.page(orderPageRequest.getPage(), orderPageRequest.getSize(), accountNo, orderPageRequest.getState());
    }

    @Override
    public String queryProductOrderState(String outTradeNo) {
        Long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();
        ProductOrderDO productOrderDO = productOrderManager.findByOutTradeNoAndAccountNo(outTradeNo, accountNo);
        if(productOrderDO == null){
            return null;
        }else{
            return productOrderDO.getState();
        }
    }

    /**
     *不涉及到商品库存管理
     *
     *业务流程
     * - 重防提交（TODO）
     * - 获取最新的流量包价格
     * - 订单验价
     *   - 如果有优惠券或者其他抵扣
     *   - 验证前端显示和后台计算价格
     * - 创建订单对象保存数据库
     * - 发送延迟消息-用于自动关单（TODO）
     * - 创建支付信息-对接三方支付（TODO）
     * - 回调更新订单状态（TODO）
     * - 支付成功创建流量包（TODO）
     */
    @Transactional
    @Override
    public JsonData confirmOrder(ConfirmOrderRequest request) {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        String orderOutTradeNo = CommonUtil.getStringNumRandom(32);

        //获取最新的流量包价格
        ProductDO productDO = productManager.findDetailById(request.getProductId());

        //验证价格
        this.checkPrice(productDO, request);

        //创建订单
        ProductOrderDO productOrderDO = this.saveProductOrder(request, loginUser, orderOutTradeNo, productDO);

        //创建支付信息-对接三方支付
        PayInfoVO payInfoVO = PayInfoVO.builder().accountNo(loginUser.getAccountNo()).outTradeNo(orderOutTradeNo)
                .clientType(request.getClientType()).payType(request.getPayType())
                .title(productDO.getTitle()).description(productDO.getTitle())
                .payFee(request.getPayAmount()).orderPayTimeoutMills(TimeConstant.ORDER_PAY_TIMEOUT_MILLS).build();

        //发送延迟消息,用于关单
        EventMessage eventMessage = EventMessage.builder()
                .eventMessageType(EventMessageType.PRODUCT_ORDER_NEW.name()).accountNo(loginUser.getAccountNo())
                .bizId(orderOutTradeNo).build();

        rabbitTemplate.convertAndSend(rabbitMQConfig.getOrderEventExchange(), rabbitMQConfig.getOrderCloseDelayRoutingKey(),  eventMessage);

        //调用支付信息
        String codeUrl = payFactory.pay(payInfoVO);
        if(StringUtils.isNotBlank(codeUrl)){
            Map<String, String> resultMap = new HashMap<>();
            //将二维码链接和订单号响应给前端
            resultMap.put("code_url", codeUrl);
            resultMap.put("out_trade_no", payInfoVO.getOutTradeNo());
            return JsonData.buildSuccess(resultMap);
        }
        return JsonData.buildResult(BizCodeEnum.PAY_ORDER_FAIL);
    }

    /**
     * //延迟消息的时间 需要比订单过期 时间长一点，这样就不存在查询的时候，用户还能支付成功
     *
     * //查询订单是否存在，如果已经支付则正常结束
     * //如果订单未支付，主动调用第三方支付平台查询订单状态
     *     //确认未支付，本地取消订单
     *     //如果第三方平台已经支付，主动的把订单状态改成已支付，造成该原因的情况可能是支付通道回调有问题，然后触发支付后的动作，如何触发？RPC还是？
     */
    @Override
    public boolean closeProductOrder(EventMessage eventMessage) {
        String outTradeNo = eventMessage.getBizId();
        Long accountNo = eventMessage.getAccountNo();

        ProductOrderDO productOrderDO = productOrderManager.findByOutTradeNoAndAccountNo(outTradeNo, accountNo);
        if(productOrderDO == null){
            //订单不存在
            log.warn("订单不存在");
            return true;
        }

        if(productOrderDO.getState().equalsIgnoreCase(ProductOrderStateEnum.PAY.name())){
            //已经支付
            log.info("直接确认消息，订单已经支付：{}",eventMessage);
            return true;
        }

        //未支付，需要向第三方支付平台查询状态
        if(productOrderDO.getState().equalsIgnoreCase(ProductOrderStateEnum.NEW.name())){
            //向第三方支付平台查询状态
            PayInfoVO payInfoVO = new PayInfoVO();
            payInfoVO.setPayType(productOrderDO.getPayType());
            payInfoVO.setOutTradeNo(outTradeNo);
            payInfoVO.setAccountNo(accountNo);

            //TODO 需要向第三方支付平台查询状态
            String payResult = "";
            if(StringUtils.isBlank(payResult)){
                //如果为空，则未支付成功，本地取消订单
                productOrderManager.updateOrderPayState(outTradeNo,accountNo,ProductOrderStateEnum.CANCEL.name(),ProductOrderStateEnum.NEW.name());
                log.info("未支付成功，本地取消订单:{}",eventMessage);
            }else {
                //支付成功，主动把订单状态更新成支付
                log.warn("支付成功，但是微信回调通知失败，需要排查问题:{}",eventMessage);
                productOrderManager.updateOrderPayState(outTradeNo,accountNo,ProductOrderStateEnum.PAY.name(),ProductOrderStateEnum.NEW.name());

                //触发支付成功后的逻辑， TODO
            }
        }

        return true;
    }

    /**
     * 支付通知结果更新订单状态
     * @param payType
     * @param paramsMap
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public JsonData handlerOrderCallbackMsg(ProductOrderPayTypeEnum payType, Map<String, String> paramsMap) {
        //获取商户订单号
        String outTradeNo = paramsMap.get("out_trade_no");
        //交易状态
        String tradeState = paramsMap.get("trade_state");
        Long accountNo = Long.valueOf(paramsMap.get("account_no"));

        /**
         * 由业务性能决定哪种方式,空间换时间，时间换空间
         *  方式一：商品信息/订单信息 可以由消费者那边 远程调用feign进行获取，多了一次开销
         *  方式二：订单生成，商品信息进行快照存储到订单（空间换时间，推荐）
         *
         */
        ProductOrderDO productOrderDO = productOrderManager.findByOutTradeNoAndAccountNo(outTradeNo,accountNo);

        Map<String, Object> content = new HashMap<>(4);
        content.put("outTradeNo", outTradeNo);
        content.put("buyNum", productOrderDO.getBuyNum());
        content.put("accountNo", productOrderDO.getAccountNo());
        //下单时存储的商品快照
        content.put("product", productOrderDO.getProductSnapshot());

        EventMessage eventMessage = EventMessage.builder()
                .bizId(outTradeNo)
                .accountNo(accountNo)
                .messageId(outTradeNo)
                .content(JsonUtil.obj2Json(content))
                .eventMessageType(EventMessageType.PRODUCT_ORDER_PAY.name())
                .build();

        if (payType.name().equalsIgnoreCase(ProductOrderPayTypeEnum.ALI_PAY.name())) {
            //支付宝支付 TODO
        } else if (payType.name().equalsIgnoreCase(ProductOrderPayTypeEnum.WECHAT_PAY.name())) {
            /**
             * 交易状态，枚举值：
             * SUCCESS：支付成功
             * REFUND：转入退款
             * NOTPAY：未支付
             * CLOSED：已关闭
             * REVOKED：已撤销（付款码支付）
             * USERPAYING：用户支付中（付款码支付）
             * PAYERROR：支付失败(其他原因，如银行返回失败)
             */
            if ("SUCCESS".equalsIgnoreCase(tradeState)) {
                //支付成功之后发送消息
                //使用redis避免生产者重复投递，确保发送消息的幂等性
                //微信24h内会重复推送，这里redis有效期设置为3天
                boolean flag = redisTemplate.opsForValue().setIfAbsent(outTradeNo, "OK", 3, TimeUnit.DAYS);
                if(flag){
                    rabbitTemplate.convertAndSend(rabbitMQConfig.getOrderEventExchange(),
                        rabbitMQConfig.getOrderUpdateTrafficRoutingKey(), eventMessage);
                    return JsonData.buildSuccess();
                }
            }
        }
        return JsonData.buildResult(BizCodeEnum.PAY_ORDER_CALLBACK_NOT_SUCCESS);
    }

    @Override
    public void handleProductOrderMessage(EventMessage eventMessage) {
        String messageType = eventMessage.getEventMessageType();
        try{
            if(EventMessageType.PRODUCT_ORDER_NEW.name().equalsIgnoreCase(messageType)){
                //关闭订单
                this.closeProductOrder(eventMessage);
            } else if(EventMessageType.PRODUCT_ORDER_PAY.name().equalsIgnoreCase(messageType)){
                //订单已经支付，更新订单状态
                String outTradeNo = eventMessage.getBizId();
                Long accountNo = eventMessage.getAccountNo();
                int rows = productOrderManager.updateOrderPayState(outTradeNo, accountNo,
                        ProductOrderStateEnum.PAY.name(),ProductOrderStateEnum.NEW.name());
                log.info("订单更新成功:rows={},eventMessage={}",rows,eventMessage);
            }
        }catch (Exception e){
            log.error("订单消费者消费失败:{}",eventMessage);
            throw new BizException(BizCodeEnum.MQ_CONSUME_EXCEPTION);
        }
    }

    private ProductOrderDO saveProductOrder(ConfirmOrderRequest request, LoginUser loginUser, String orderOutTradeNo, ProductDO productDO) {
        ProductOrderDO productOrderDO = new ProductOrderDO();
        //设置用户信息
        productOrderDO.setAccountNo(loginUser.getAccountNo());
        productOrderDO.setNickname(loginUser.getUsername());

        //设置商品信息
        productOrderDO.setProductId(productDO.getId());
        productOrderDO.setProductTitle(productDO.getTitle());
        productOrderDO.setProductSnapshot(JsonUtil.obj2Json(productDO));
        productOrderDO.setProductAmount(productDO.getAmount());

        //设置订单信息
        productOrderDO.setBuyNum(request.getBuyNum());
        productOrderDO.setOutTradeNo(orderOutTradeNo);
        productOrderDO.setCreateTime(new Date());
        productOrderDO.setDel(0);

        //发票信息
        productOrderDO.setBillType(BillTypeEnum.valueOf(request.getBillType()).name());
        productOrderDO.setBillHeader(request.getBillHeader());
        productOrderDO.setBillReceiverPhone(request.getBillReceiverPhone());
        productOrderDO.setBillReceiverEmail(request.getBillReceiverEmail());
        productOrderDO.setBillContent(request.getBillContent());

        //实际支付总价
        productOrderDO.setTotalAmount(request.getPayAmount());
        //总价，没使用优惠券
        productOrderDO.setTotalAmount(request.getTotalAmount());
        //订单状态
        productOrderDO.setState(ProductOrderStateEnum.NEW.name());
        //支付类型
        productOrderDO.setPayType(ProductOrderPayTypeEnum.valueOf(request.getPayType()).name());

        //插入数据库
        productOrderManager.add(productOrderDO);
        return productOrderDO;
    }

    private void checkPrice(ProductDO productDO, ConfirmOrderRequest request) {
        //后端计算价格
        BigDecimal bizTotal = BigDecimal.valueOf(request.getBuyNum()).multiply(productDO.getAmount());
        //前端传递总价和后端计算总价格是否一致,如果有优惠券，也在这里进行计算
        if(bizTotal.compareTo(request.getPayAmount()) != 0){
            log.error("验证价格失败{}",request);
            throw new BizException(BizCodeEnum.ORDER_CONFIRM_PRICE_FAIL);
        }
    }
}
