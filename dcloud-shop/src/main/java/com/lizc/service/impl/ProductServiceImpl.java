package com.lizc.service.impl;

import com.lizc.manager.ProductManager;
import com.lizc.model.ProductDO;
import com.lizc.service.ProductService;
import com.lizc.vo.ProductVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizengcai
 * @since 2024-04-05
 */
@Service
public class ProductServiceImpl implements ProductService {


    @Autowired
    private ProductManager productManager;

    @Override
    public List<ProductVO> list() {
        List<ProductDO> productDOList = productManager.list();
        List<ProductVO> productVOList = productDOList.stream()
                .map(productDO ->  beanProcess(productDO))
                .collect(Collectors.toList());
        return productVOList;
    }

    @Override
    public ProductVO findDetailById(long productId) {
        ProductDO productDO = productManager.findDetailById(productId);
        return beanProcess(productDO);
    }


    private ProductVO beanProcess(ProductDO productDO) {
        ProductVO productVO = new ProductVO();
        BeanUtils.copyProperties(productDO, productVO);
        return productVO;
    }
}
