package com.lizc.service;

import com.lizc.vo.ProductVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizengcai
 * @since 2024-04-05
 */
public interface ProductService {

    List<ProductVO> list();


    ProductVO findDetailById(long productId);
}
