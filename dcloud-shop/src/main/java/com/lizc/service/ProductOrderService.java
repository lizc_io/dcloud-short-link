package com.lizc.service;

import com.lizc.controller.request.ConfirmOrderRequest;
import com.lizc.controller.request.ProductOrderPageRequest;
import com.lizc.enums.ProductOrderPayTypeEnum;
import com.lizc.model.EventMessage;
import com.lizc.util.JsonData;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizengcai
 * @since 2024-04-05
 */
public interface ProductOrderService {

    Map<String, Object> page(ProductOrderPageRequest orderPageRequest);

    String queryProductOrderState(String outTradeNo);

    JsonData confirmOrder(ConfirmOrderRequest confirmOrderRequest);

    boolean closeProductOrder(EventMessage eventMessage);

    JsonData handlerOrderCallbackMsg(ProductOrderPayTypeEnum productOrderPayTypeEnum, Map<String, String> paramsMap);

    void handleProductOrderMessage(EventMessage eventMessage);
}
