package com.lizc.listener;

import com.lizc.enums.BizCodeEnum;
import com.lizc.exception.BizException;
import com.lizc.model.EventMessage;
import com.lizc.service.ProductOrderService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
@RabbitListener(queuesToDeclare = {
        @Queue("order.close.queue"), //关闭订单队列
        @Queue("order.update.queue")  //更新订单状态队列
})
public class ProductOrderMQListener {

    @Autowired
    private ProductOrderService productOrderService;

    @RabbitHandler
    public void productOrderHandler(EventMessage eventMessage, Message message, Channel channel) throws IOException {
        log.info("监听到消息ProductOrderMQListener message消息内容:{}", message);
        try {
            productOrderService.handleProductOrderMessage(eventMessage);
        } catch (Exception e) {
            //处理业务异常，还有进行其他操作，比如记录失败原因
            log.error("消费失败:{}", eventMessage);
            throw new BizException(BizCodeEnum.MQ_CONSUME_EXCEPTION);
        }
        log.info("消费成功:{}", eventMessage);
    }

}