package com.lizc.config;

public class WechatPayApi {

    /**
     * 微信支付域名
     */
    public static final String HOST = "https://api.mch.weixin.qq.com";

    /**
     * Native下单
     */
    public static final String NATIVE_ORDER = HOST+"/v3/pay/transactions/native";

    /**
     * Native订单查询，根据商户订单号查询，out_trade_no  GET请求
     */
    public static final String NATIVE_QUERY = HOST+"/v3/pay/transactions/out-trade-no/%s?mchid=%s";

    /**
     * 关闭订单, POST方式 根据 out_trade_no
     */
    public static final String NATIVE_CLOSE_ORDER = HOST+"/v3/pay/transactions/out-trade-no/%s/close";


    /**
     * 申请退款接口 POST方式
     */
    public static final String NATIVE_REFUND_ORDER = HOST+"/v3/refund/domestic/refunds";


    /**
     * 查询单笔退款API GET方式
     */
    public static final String NATIVE_REFUND_QUERY = HOST+"/v3/refund/domestic/refunds/%s";




}
