package com.lizc.manager;

import com.lizc.model.ProductDO;
import com.lizc.vo.ProductVO;

import java.util.List;

/**
 * @author lizengcai
 * @version v1.0
 * @className ProductManager
 * @description TODO
 * @date 2024/4/5 22:32
 */
public interface ProductManager {
    List<ProductDO> list();

    ProductDO findDetailById(long productId);
}
