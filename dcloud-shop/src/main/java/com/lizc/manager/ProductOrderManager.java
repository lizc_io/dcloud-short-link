package com.lizc.manager;

import com.lizc.model.ProductOrderDO;

import java.util.Map;

/**
 * @author lizengcai
 * @version v1.0
 * @className ProductOrderManager
 * @description TODO
 * @date 2024/4/5 23:05
 */
public interface ProductOrderManager {


    int add(ProductOrderDO productOrderDO);

    ProductOrderDO findByOutTradeNoAndAccountNo(String outTradeNo, Long accountNo);

    int updateOrderPayState(String outTradeNo, Long accountNo, String newState, String oldState);

    Map<String, Object> page(int page, int size, Long accountNo, String state);


    int del(long productOrderId, Long accountNo);
}
