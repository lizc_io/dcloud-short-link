package com.lizc.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lizc.manager.ProductOrderManager;
import com.lizc.mapper.ProductOrderMapper;
import com.lizc.model.ProductDO;
import com.lizc.model.ProductOrderDO;
import com.lizc.vo.ProductOrderVO;
import com.lizc.vo.ProductVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lizengcai
 * @version v1.0
 * @className ProductOrderManagerImpl
 * @description TODO
 * @date 2024/4/5 23:05
 */
@Component
public class ProductOrderManagerImpl implements ProductOrderManager {

    @Autowired
    private ProductOrderMapper productOrderMapper;

    @Override
    public int add(ProductOrderDO productOrderDO) {
        return productOrderMapper.insert(productOrderDO);
    }

    @Override
    public ProductOrderDO findByOutTradeNoAndAccountNo(String outTradeNo, Long accountNo) {
        ProductOrderDO productOrderDO = productOrderMapper.selectOne(new QueryWrapper<ProductOrderDO>()
                .eq("out_trade_no", outTradeNo)
                .eq("account_no", accountNo)
                .eq("del", 0));
        return productOrderDO;
    }

    @Override
    public int updateOrderPayState(String outTradeNo, Long accountNo, String newState, String oldState) {
        int rows = productOrderMapper.update(null, new UpdateWrapper<ProductOrderDO>()
                .eq("out_trade_no", outTradeNo)
                .eq("account_no", accountNo)
                .eq("state", oldState)

                .set("state", newState));
        return rows;
    }

    @Override
    public Map<String, Object> page(int page, int size, Long accountNo, String state) {
        Page<ProductOrderDO> pageInfo = new Page<>(page, size);
        IPage<ProductOrderDO> result;
        if(StringUtils.isBlank(state)){
            result = productOrderMapper.selectPage(pageInfo, new QueryWrapper<ProductOrderDO>()
                    .eq("account_no", accountNo)
                    .eq("del", 0));
        }else{
            result = productOrderMapper.selectPage(pageInfo, new QueryWrapper<ProductOrderDO>()
                    .eq("account_no", accountNo)
                    .eq("state", state)
                    .eq("del", 0));
        }

        List<ProductOrderDO> productOrderDOList = result.getRecords();
        List<ProductOrderVO> productOrderVOList = productOrderDOList.stream().map(obj -> beanProcess(obj)).collect(Collectors.toList());
        Map<String, Object> pageMap = new HashMap<>(4);
        pageMap.put("total_page", result.getPages());
        pageMap.put("total_record", result.getTotal());
        pageMap.put("current_data", productOrderVOList);
        return pageMap;
    }

    @Override
    public int del(long productOrderId, Long accountNo) {
        int rows = productOrderMapper.update(null, new UpdateWrapper<ProductOrderDO>()
                .eq("id", productOrderId)
                .eq("account_no", accountNo)
                .set("del", 1));
        return rows;
    }

    private ProductOrderVO beanProcess(ProductOrderDO productOrderDO) {
        ProductOrderVO productOrderVO = new ProductOrderVO();
        BeanUtils.copyProperties(productOrderDO, productOrderVO);
        return productOrderVO;
    }
}
