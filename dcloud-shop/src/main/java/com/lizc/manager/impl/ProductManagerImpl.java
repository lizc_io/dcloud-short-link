package com.lizc.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lizc.manager.ProductManager;
import com.lizc.mapper.ProductMapper;
import com.lizc.model.ProductDO;
import com.lizc.vo.ProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lizengcai
 * @version v1.0
 * @className ProductManagerImpl
 * @description TODO
 * @date 2024/4/5 22:32
 */
@Component
public class ProductManagerImpl implements ProductManager {

    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<ProductDO> list() {
        List<ProductDO> productDOList = productMapper.selectList(new QueryWrapper<ProductDO>());
        return productDOList;
    }

    @Override
    public ProductDO findDetailById(long productId) {
        return productMapper.selectOne(new QueryWrapper<ProductDO>().eq("id", productId));
    }
}
