package com.lizc.component;


import com.lizc.vo.PayInfoVO;

/**
 * 支付策略 抽象类
 */
public interface PayStrategy {

    /**
     * 统一下单接口
     * @param payInfoVO
     * @return
     */
    String unifiedOrder(PayInfoVO payInfoVO);

    /**
     * 退款接口
     * @param payInfoVO
     * @return
     */
    default String refund(PayInfoVO payInfoVO){
        return "";
    }


    /**
     * 查询支付状态
     * @param payInfoVO
     * @return
     */
    default String queryPayStatus(PayInfoVO payInfoVO){
        return "";
    }

    /**
     *  关闭订单
     */
    default String closeOrder(PayInfoVO payInfoVO){
        return "";
    }

}
