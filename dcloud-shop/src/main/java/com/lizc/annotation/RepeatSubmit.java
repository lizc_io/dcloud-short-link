package com.lizc.annotation;

import java.lang.annotation.*;

/**
 * @author lizengcai
 * @version v1.0
 * @className RepeatSubmit
 * @description TODO
 * @date 2024/4/6 1:54
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RepeatSubmit {

    /**
     * 加锁过期时间，默认是5秒
     * @return
     */
    long lockTime() default 5;

    /**
     * 默认限制类型，是方法参数
     * @return
     */
    Type limitType() default Type.PARAM;

    /**
     * 两种类型，token 或者 param
     */
    enum  Type{ PARAM , TOKEN};
}
