package com.lizc.controller;


import com.lizc.annotation.RepeatSubmit;
import com.lizc.constant.RedisKey;
import com.lizc.controller.request.ConfirmOrderRequest;
import com.lizc.controller.request.ProductOrderPageRequest;
import com.lizc.enums.BizCodeEnum;
import com.lizc.enums.ClientTypeEnum;
import com.lizc.enums.ProductOrderPayTypeEnum;
import com.lizc.interceptor.LoginInterceptor;
import com.lizc.model.LoginUser;
import com.lizc.service.ProductOrderService;
import com.lizc.util.CommonUtil;
import com.lizc.util.JsonData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizengcai
 * @since 2024-04-05
 */
@Slf4j
@RestController
@RequestMapping("/api/order/v1")
public class ProductOrderController {

    @Autowired
    private ProductOrderService productOrderService;

    @Autowired
    private StringRedisTemplate redisTemplate;


    @GetMapping("token")
    public JsonData getToken(){
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        String token = CommonUtil.getStringNumRandom(32);
        //"order:submit:%s:%s"
        String key = String.format(RedisKey.SUBMIT_ORDER_TOKEN_KEY, loginUser.getAccountNo(), token);
        //令牌有效时间是30分钟
        redisTemplate.opsForValue().set(key, "1", 30, TimeUnit.MINUTES);
        return JsonData.buildSuccess(token);
    }


    @PostMapping("page")
    public JsonData page(@RequestBody ProductOrderPageRequest productOrderPageRequest){
        Map<String, Object> pageResult = productOrderService.page(productOrderPageRequest);
        return JsonData.buildSuccess(pageResult);
    }

    @GetMapping("query_state")
    public JsonData queryState(@RequestParam(value = "outTradeNo") String outTradeNo){
        String state = productOrderService.queryProductOrderState(outTradeNo);
        return StringUtils.isBlank(state) ?
                JsonData.buildResult(BizCodeEnum.ORDER_CONFIRM_NOT_EXIST) : JsonData.buildSuccess(state);
    }

    @RepeatSubmit(limitType = RepeatSubmit.Type.PARAM)
    @PostMapping("confirm")
    public void confirmOrder(@RequestBody ConfirmOrderRequest confirmOrderRequest, HttpServletResponse response){
        JsonData jsonData = productOrderService.confirmOrder(confirmOrderRequest);
        if(jsonData.getCode() == 0){
            //创建订单成功
            //端类型
            String clientType = confirmOrderRequest.getClientType();
            //支付类型
            String payType = confirmOrderRequest.getPayType();

            //如果是支付宝支付
            if(payType.equalsIgnoreCase(ProductOrderPayTypeEnum.ALI_PAY.name())){
                //PC 跳转到网页， 移动端返回json
                if(clientType.equalsIgnoreCase(ClientTypeEnum.PC.name())){
                    CommonUtil.sendHtmlMessage(response, jsonData);
                } else if(clientType.equalsIgnoreCase(ClientTypeEnum.APP.name())){
                    
                } else if(clientType.equalsIgnoreCase(ClientTypeEnum.H5.name())){

                }
            }else if(payType.equalsIgnoreCase(ProductOrderPayTypeEnum.WECHAT_PAY.name())){
                //如果是微信支付，直接返回json串
                CommonUtil.sendJsonMessage(response, jsonData);
            }
        }else{
            log.error("创建订单失败:{}", jsonData);
            CommonUtil.sendJsonMessage(response, jsonData);
        }
    }



}

