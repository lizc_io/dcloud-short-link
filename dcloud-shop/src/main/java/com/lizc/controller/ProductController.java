package com.lizc.controller;


import com.lizc.service.ProductService;
import com.lizc.util.JsonData;
import com.lizc.vo.ProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizengcai
 * @since 2024-04-05
 */
@RestController
@RequestMapping("/api/product/v1")
public class ProductController {

    @Autowired
    private ProductService productService;


    /**
     * 查看商品列表接口
     */
    @GetMapping("list")
    private JsonData list(){
        List<ProductVO> list = productService.list();
        return JsonData.buildSuccess(list);
    }

    /**
     * 查看商品列表接口
     */
    @GetMapping("detail/{product_id}")
    private JsonData detail(@PathVariable("product_id") long productId){
        ProductVO productVO = productService.findDetailById(productId);
        return JsonData.buildSuccess(productVO);
    }

}

