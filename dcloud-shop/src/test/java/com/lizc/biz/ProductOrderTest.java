package com.lizc.biz;

import com.lizc.ShopApplication;
import com.lizc.manager.ProductOrderManager;
import com.lizc.model.ProductOrderDO;
import com.lizc.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author lizengcai
 * @version v1.0
 * @className ProductOrderTest
 * @description TODO
 * @date 2024/4/5 23:43
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShopApplication.class)
@Slf4j
public class ProductOrderTest {

    @Autowired
    private ProductOrderManager productOrderManager;

    @Test
    public void testAdd(){
        for(long i=0; i<5;i++){
            ProductOrderDO productOrderDO = ProductOrderDO.builder()
                    .outTradeNo(CommonUtil.generateUUID())
                    .payAmount(new BigDecimal(11))
                    .state("NEW")
                    .nickname("lizc i=" + i)
                    .accountNo(i)
                    .del(0)
                    .productId(2L).build();
            productOrderManager.add(productOrderDO);
        }
    }
    @Test
    public void testPage(){
        Map<String, Object> pageResult = productOrderManager.page(1,2, 10L, null);
        log.info(pageResult.toString());
    }

}
