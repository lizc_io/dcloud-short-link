package com.lizc.biz;

import com.alibaba.fastjson.JSONObject;
import com.lizc.ShopApplication;
import com.lizc.config.PayBeanConfig;
import com.lizc.config.WechatPayApi;
import com.lizc.config.WechatPayConfig;
import com.lizc.manager.ProductOrderManager;
import com.lizc.model.ProductOrderDO;
import com.lizc.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author lizengcai
 * @version v1.0
 * @className WechatPayTest
 * @description TODO
 * @date 2024/4/5 23:43
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShopApplication.class)
@Slf4j
public class WechatPayTest {

    @Autowired
    private PayBeanConfig payBeanConfig;

    @Autowired
    private WechatPayConfig payConfig;

    //微信HTTP客户端指定名称注入
    @Qualifier("wechatPayClient")
    @Autowired
    private CloseableHttpClient wechatPayClient;

    @Test
    public void testLoadPrivateKey() throws IOException {
        log.info(payBeanConfig.getPrivateKey().getAlgorithm());
    }

    /**
     * 快速验证Native 统一下单
     */
    @Test
    public void testNativeOrder(){
        /**
         * {
         * 	"mchid": "1900006XXX",
         * 	"out_trade_no": "native12177525012014070332333",
         * 	"appid": "wxdace645e0bc2cXXX",
         * 	"description": "Image形象店-深圳腾大-QQ公仔",
         * 	"notify_url": "https://weixin.qq.com/",
         * 	"amount": {
         * 		"total": 1,
         * 		"currency": "CNY"
         * 	    }
         * }
         */
        //过期时间  RFC 3339格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        //支付订单过期时间
        String timeExpire = sdf.format(new Date(System.currentTimeMillis() + 6000 * 60 * 1000));
        String outTradeNo = CommonUtil.getStringNumRandom(32);

        JSONObject amountObj = new JSONObject();
        JSONObject payObj = new JSONObject();
        payObj.put("mchid", payConfig.getMchId());
        payObj.put("out_trade_no", outTradeNo);
        payObj.put("appid", payConfig.getWxPayAppid());
        payObj.put("description", "小滴课堂海量数据项目大课");
        payObj.put("notify_url", payConfig.getCallbackUrl());
        payObj.put("time_expire", timeExpire);

        //微信支付需要以分为单位
        int amount = 100;
        amountObj.put("total", amount);
        amountObj.put("currency", "CNY");
        payObj.put("amount", amountObj);

        //附属参数，可以用在回调时携带传给调用方
        payObj.put("attach", "{\"accountNo\":" + 8888 + "}");

        // 处理请求body参数
        String body = payObj.toJSONString();
        log.info("请求参数:{}", payObj);
        StringEntity entity = new StringEntity(body, "utf-8");
        entity.setContentType("application/json");

        //调用统一下单API
        HttpPost httpPost = new HttpPost(WechatPayApi.NATIVE_ORDER);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setEntity(entity);

        //使用微信httpClient自动进行参数签名
        try (CloseableHttpResponse response = wechatPayClient.execute(httpPost)) {
            String responseStr = EntityUtils.toString(response.getEntity());
            //响应体
            int statusCode = response.getStatusLine().getStatusCode();
            log.info("统一下单响应码={}，响应体={}", statusCode, responseStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testWechatPayNativeQuery() throws IOException {

        String outTradeNo = "BaFza7Ol9OpHxb2hiOy2lurKF3ubuYLW";
        String url = String.format(WechatPayApi.NATIVE_QUERY, outTradeNo, payConfig.getMchId());

        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Accept", "application/json");

        //httpClient自动进行参数签名
        try (CloseableHttpResponse response = wechatPayClient.execute(httpGet)) {
            String responseStr = EntityUtils.toString(response.getEntity());
            int statusCode = response.getStatusLine().getStatusCode();
            log.info("查询订单响应码={}，响应体={}", statusCode, responseStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testWechatPayNativeCloseOrder() throws IOException {

        String outTradeNo = "99lHMc8uO2ZZ03KSV24XEFaLRfSMfqwD";
        String url = String.format(WechatPayApi.NATIVE_CLOSE_ORDER, outTradeNo, payConfig.getMchId());

        HttpPost httpPost = new HttpPost(url);

        //组装json
        JSONObject payObj = new JSONObject();

        payObj.put("mchid", payConfig.getMchId());
        String body = payObj.toJSONString();

        log.info("请求参数={}", body);

        //将请求参数设置到请求对象中
        StringEntity entity = new StringEntity(body, "utf-8");
        entity.setContentType("application/json");
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");

        //httpClient自动进行参数签名
        try (CloseableHttpResponse response = wechatPayClient.execute(httpPost);) {
            //响应状态码 Http状态码为204表示成功
            int statusCode = response.getStatusLine().getStatusCode();
            log.info("关闭订单响应码={}，无响应体", statusCode);  //关闭订单响应码=204，无响应体

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * 订单退款
     *
     * @throws IOException
     */
    @Test
    public void testNativeRefundOrder() throws IOException {

        //订单号
        String outTradeNo = "BBnMJT4zaReyMLU9hi0QAfhXIcwb5pjS";

        //退款订单号
        String refundNo = CommonUtil.getStringNumRandom(32);

        // 请求body参数
        JSONObject refundObj = new JSONObject();
        //订单号
        refundObj.put("out_trade_no", outTradeNo);
        //退款单编号，商户系统内部的退款单号，商户系统内部唯一，
        // 只能是数字、大小写字母_-|*@ ，同一退款单号多次请求只退一笔
        refundObj.put("out_refund_no", refundNo);
        refundObj.put("reason","这里是商品描述");
        //refundObj.put("notify_url", payConfig.getCallbackUrl());

        JSONObject amountObj = new JSONObject();
        //退款金额
        amountObj.put("refund", 100);
        //实际支付的总金额
        amountObj.put("total", 100);
        amountObj.put("currency", "CNY");

        refundObj.put("amount", amountObj);

        String body = refundObj.toJSONString();
        log.info("请求参数:{}",body);

        StringEntity entity = new StringEntity(body,"utf-8");
        entity.setContentType("application/json");

        //调用统一退款 API
        HttpPost httpPost = new HttpPost(WechatPayApi.NATIVE_REFUND_ORDER);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setEntity(entity);

        try(CloseableHttpResponse response = wechatPayClient.execute(httpPost)){
            //响应码
            int statusCode = response.getStatusLine().getStatusCode();
            //响应体
            String responseStr = EntityUtils.toString(response.getEntity());

            log.info("退款响应码:{},响应体:{}",statusCode,responseStr);

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * {"amount":{"currency":"CNY","discount_refund":0,"from":[],"payer_refund":10,
     * "payer_total":100,"refund":10,"settlement_refund":10,
     * "settlement_total":100,"total":100},"channel":"ORIGINAL",
     * "create_time":"2022-01-18T13:14:46+08:00","funds_account":"AVAILABLE",
     * "out_refund_no":"Pe9rWbRpUDu51PFvo8L17LJZHm6dpbj7",
     * "out_trade_no":"6xYsHV3UziDINu06B0XeuzmNvOedjhY5","promotion_detail":[],
     * "refund_id":"50302000542022011816569235991","status":"SUCCESS",
     * "success_time":"2022-01-18T13:14:55+08:00","transaction_id":"4200001390202201189710793189",
     * "user_received_account":"民生银行信用卡5022"}
     * @throws IOException
     */
    @Test
    public void testNativeRefundQuery() throws IOException {
        //退款申请单号
        String refundNo = "Pe9rWbRpUDu51PFvo8L17LJZHm6dpbj7";

        String url = String.format(WechatPayApi.NATIVE_REFUND_QUERY, refundNo);
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Accept","application/json");

        try(CloseableHttpResponse response = wechatPayClient.execute(httpGet)){
            //响应码
            int statusCode = response.getStatusLine().getStatusCode();
            //响应体
            String responseStr = EntityUtils.toString(response.getEntity());

            log.info("查询退款响应码:{},响应体:{}",statusCode,responseStr);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
