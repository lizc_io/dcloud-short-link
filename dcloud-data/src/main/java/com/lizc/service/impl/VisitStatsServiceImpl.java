package com.lizc.service.impl;

import com.lizc.controller.request.BaseQueryRequest;
import com.lizc.controller.request.RegionQueryRequest;
import com.lizc.controller.request.VisitRecordPageRequest;
import com.lizc.controller.request.VisitTrendQueryRequest;
import com.lizc.enums.DateTimeFieldEnum;
import com.lizc.enums.QueryDeviceEnum;
import com.lizc.interceptor.LoginInterceptor;
import com.lizc.mapper.VisitStatsMapper;
import com.lizc.model.VisitStatsDO;
import com.lizc.service.VisitStatsService;
import com.lizc.vo.VisitStatsVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class VisitStatsServiceImpl implements VisitStatsService {


    @Autowired
    private VisitStatsMapper visitStatsMapper;

    @Override
    public Map<String, Object> pageVisitRecord(VisitRecordPageRequest request) {
        Long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();

        Map<String, Object> data = new HashMap<>(16);

        String code = request.getCode();
        int page = request.getPage();
        int size = request.getSize();

        int count = visitStatsMapper.countTotal(code, accountNo);
        int from = (page - 1) * size;

        List<VisitStatsDO> visitStatsDOList = visitStatsMapper.pageVisitRecord(code, accountNo, from, size);
        List<VisitStatsVO> visitStatsVOList = visitStatsDOList.stream().map(v -> beanProcess(v)).collect(Collectors.toList());
        data.put("total", count);
        data.put("current_page", page);

        /**计算总页数*/
        int totalPage = 0;
        if (count % size == 0) {
            totalPage = count / size;
        } else {
            totalPage = count / size + 1;
        }
        data.put("total_page", totalPage);

        data.put("data", visitStatsVOList);
        return data;
    }

    //时间先不做限制，正常需要限制
    @Override
    public List<VisitStatsVO> queryRegionVisitStatsWithDay(RegionQueryRequest request) {
        Long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();
        List<VisitStatsDO> visitStatsDOList = visitStatsMapper.queryRegionVisitStatsWithDay(request.getCode(), request.getStartTime(),
                request.getEndTime(), accountNo);
        List<VisitStatsVO> visitStatsVOList = visitStatsDOList.stream().map(v -> beanProcess(v)).collect(Collectors.toList());
        return visitStatsVOList;
    }

    /**
     * 查询访问趋势，支持多天查询，支持查询当天小时级别
     *
     * @param request
     * @return
     */
    @Override
    public List<VisitStatsVO> queryVisitTrend(VisitTrendQueryRequest request) {

        Long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();
        String code = request.getCode();
        String type = request.getType();

        List<VisitStatsDO> list = null;
        if (DateTimeFieldEnum.DAY.name().equalsIgnoreCase(type)) {
            list = visitStatsMapper.queryVisitTrendWithMultiDay(code, accountNo, request.getStartTime(), request.getEndTime());

        }
        if (DateTimeFieldEnum.DAY.name().equalsIgnoreCase(type)) {
            list = visitStatsMapper.queryVisitTrendWithMultiDay(code, accountNo, request.getStartTime(), request.getEndTime());

        }
        List<VisitStatsVO> visitStatsVOS = list.stream().map(obj -> beanProcess(obj)).collect(Collectors.toList());

        return visitStatsVOS;
    }


    /**
     * 高频访问来源
     *
     * @param request
     * @return
     */
    @Override
    public List<VisitStatsVO> queryFrequentReferer(BaseQueryRequest request) {
        Long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();
        List<VisitStatsDO> list = visitStatsMapper.queryFrequentReferer(request.getCode(), accountNo, 10);
        List<VisitStatsVO> visitStatsVOS = list.stream().map(obj -> beanProcess(obj)).collect(Collectors.toList());
        return visitStatsVOS;
    }

    @Override
    public Map<String, List<VisitStatsVO>> queryDeviceInfo(BaseQueryRequest request) {
        Long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();
        List<VisitStatsDO> osList = visitStatsMapper.queryDeviceInfo(request.getCode(), request.getStartTime(), request.getEndTime(), QueryDeviceEnum.OS.name());
        List<VisitStatsDO> browserList = visitStatsMapper.queryDeviceInfo(request.getCode(), request.getStartTime(), request.getEndTime(), QueryDeviceEnum.BROWSER.name());
        List<VisitStatsDO> deviceList = visitStatsMapper.queryDeviceInfo(request.getCode(), request.getStartTime(), request.getEndTime(), QueryDeviceEnum.DEVICE.name());

        List<VisitStatsVO> osVOList = osList.stream().map(obj -> beanProcess(obj)).collect(Collectors.toList());
        List<VisitStatsVO> browserVOList = browserList.stream().map(obj -> beanProcess(obj)).collect(Collectors.toList());
        List<VisitStatsVO> deviceVOList = deviceList.stream().map(obj -> beanProcess(obj)).collect(Collectors.toList());

        Map<String, List<VisitStatsVO>> map = new HashMap<>();
        map.put( QueryDeviceEnum.OS.name(), osVOList);
        map.put( QueryDeviceEnum.BROWSER.name(), browserVOList);
        map.put( QueryDeviceEnum.DEVICE.name(), deviceVOList);
        return map;
    }

    private VisitStatsVO beanProcess(VisitStatsDO visitStatsDO){
        VisitStatsVO visitStatsVO = new VisitStatsVO();
        BeanUtils.copyProperties(visitStatsDO, visitStatsVO);
        return visitStatsVO;
    }

}
