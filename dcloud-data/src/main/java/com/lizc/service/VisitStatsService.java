package com.lizc.service;

import com.lizc.controller.request.BaseQueryRequest;
import com.lizc.controller.request.RegionQueryRequest;
import com.lizc.controller.request.VisitRecordPageRequest;
import com.lizc.controller.request.VisitTrendQueryRequest;
import com.lizc.model.VisitStatsDO;
import com.lizc.vo.VisitStatsVO;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public interface VisitStatsService {
    Map<String, Object> pageVisitRecord(VisitRecordPageRequest request);

    List<VisitStatsVO> queryRegionVisitStatsWithDay(RegionQueryRequest request);

    List<VisitStatsVO> queryVisitTrend(VisitTrendQueryRequest request);

    List<VisitStatsVO> queryFrequentReferer(BaseQueryRequest request);

    Map<String, List<VisitStatsVO>> queryDeviceInfo(BaseQueryRequest request);
}
