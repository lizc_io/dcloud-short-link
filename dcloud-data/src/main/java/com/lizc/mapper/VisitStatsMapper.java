package com.lizc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lizc.model.VisitStatsDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VisitStatsMapper extends BaseMapper<VisitStatsDO> {

    int countTotal(@Param("code") String code, @Param("accountNo") Long accountNo);

    List<VisitStatsDO> pageVisitRecord(@Param("code")String code, @Param("accountNo")Long accountNo, @Param("from")int from, @Param("size")int size);

    List<VisitStatsDO> queryRegionVisitStatsWithDay(@Param("code")String code, @Param("startTime")String startTime,
                                                    @Param("endTime")String endTime, @Param("accountNo")Long accountNo);

    List<VisitStatsDO> queryVisitTrendWithMultiDay(@Param("code")String code, @Param("accountNo")Long accountNo,
                                                   @Param("startTime")String startTime, @Param("endTime")String endTime);

    List<VisitStatsDO> queryFrequentReferer(@Param("code")String code, @Param("accountNo")Long accountNo, @Param("size")int size);

    List<VisitStatsDO> queryDeviceInfo(@Param("code")String code, @Param("startTime")String startTime, @Param("endTime")String endTime, @Param("field")String field);
}
