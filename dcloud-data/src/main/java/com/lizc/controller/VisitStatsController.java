package com.lizc.controller;


import com.lizc.controller.request.BaseQueryRequest;
import com.lizc.controller.request.RegionQueryRequest;
import com.lizc.controller.request.VisitRecordPageRequest;
import com.lizc.controller.request.VisitTrendQueryRequest;
import com.lizc.enums.BizCodeEnum;
import com.lizc.model.VisitStatsDO;
import com.lizc.service.VisitStatsService;
import com.lizc.util.JsonData;
import com.lizc.vo.VisitStatsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/api/visit_stats/v1")
public class VisitStatsController {


    @Autowired
    private VisitStatsService visitStatsService;


    @RequestMapping("page_record")
    public JsonData pageVisitRecord(@RequestBody VisitRecordPageRequest request){
        //条数限制
        int total = request.getSize() * request.getPage();
        if(total>1000) {
            return JsonData.buildResult(BizCodeEnum.DATA_OUT_OF_LIMIT_SIZE);
        }
        Map<String, Object> pageResult = visitStatsService.pageVisitRecord(request);
        return JsonData.buildSuccess(pageResult);
    }

    @RequestMapping("region_day")
    public JsonData queryRegionWithDay(@RequestBody RegionQueryRequest request){
        List<VisitStatsVO> visitStatsDOList = visitStatsService.queryRegionVisitStatsWithDay(request);
        return JsonData.buildSuccess(visitStatsDOList);
    }

    @RequestMapping("trend")
    public JsonData queryVisitTrend(@RequestBody VisitTrendQueryRequest request){
        List<VisitStatsVO> visitStatsDOList = visitStatsService.queryVisitTrend(request);
        return JsonData.buildSuccess(visitStatsDOList);
    }

    @RequestMapping("frequent_source")
    public JsonData queryFrequentSource(@RequestBody BaseQueryRequest request){
        List<VisitStatsVO> visitStatsVOList = visitStatsService.queryFrequentReferer(request);
        return JsonData.buildSuccess(visitStatsVOList);
    }


    @RequestMapping("device_info")
    public JsonData queryDeviceInfo(@RequestBody BaseQueryRequest request){
        Map<String, List<VisitStatsVO>> visitStatsVOList = visitStatsService.queryDeviceInfo(request);
        return JsonData.buildSuccess(visitStatsVOList);
    }


}
