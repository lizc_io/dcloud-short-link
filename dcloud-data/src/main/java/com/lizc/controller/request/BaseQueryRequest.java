package com.lizc.controller.request;

import lombok.Data;

@Data
public class BaseQueryRequest {

    private String code;
    private String startTime;
    private String endTime;

}
