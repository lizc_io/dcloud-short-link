package com.lizc.job;


import com.lizc.service.TrafficService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TrafficJobHandler {

    @Autowired
    private TrafficService trafficService;

    @XxlJob(value = "trafficExpiredHandler",init = "init",destroy = "destroy")
    public ReturnT<String> execute(String param){
        log.info("小滴课堂 execute 任务方法触发成功, 删除过期流量包");
        trafficService.deleteExpireTraffic();
        //默认返回成功
        return ReturnT.SUCCESS;
    }

    private void init(){
        log.info("小滴课堂 TrafficJobHandler init >>>>>");
    }

    private void destroy(){
        log.info("小滴课堂 TrafficJobHandler destroy >>>>>");
    }
}
