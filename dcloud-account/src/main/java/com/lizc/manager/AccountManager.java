package com.lizc.manager;

import com.lizc.model.AccountDO;

import java.util.List;

/**
 * @author lizengcai
 * @version v1.0
 * @className AccountManager
 * @description TODO
 * @date 2024/3/23 21:19
 */
public interface AccountManager {

    int insert(AccountDO accountDO);

    List<AccountDO> findByPhone(String phone);
}
