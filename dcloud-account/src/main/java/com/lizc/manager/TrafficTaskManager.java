package com.lizc.manager;

import com.lizc.model.TrafficTaskDO;

public interface TrafficTaskManager {

    int add(TrafficTaskDO trafficTaskDO);

    TrafficTaskDO findByIdAndAccountNo(Long id, Long accountNo);

    int deleteByIdAndAccountNo(Long id, Long accountNo);
}
