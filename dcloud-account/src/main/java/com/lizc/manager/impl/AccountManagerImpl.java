package com.lizc.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lizc.manager.AccountManager;
import com.lizc.mapper.AccountMapper;
import com.lizc.model.AccountDO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lizengcai
 * @version v1.0
 * @className AccountManagerImpl
 * @description TODO
 * @date 2024/3/23 21:20
 */
@Component
@Slf4j
public class AccountManagerImpl implements AccountManager {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public int insert(AccountDO accountDO) {
        return accountMapper.insert(accountDO);
    }

    @Override
    public List<AccountDO> findByPhone(String phone) {
        List<AccountDO>  accountDOList = accountMapper.selectList(new QueryWrapper<AccountDO>().eq("phone", phone));
        return accountDOList;
    }
}
