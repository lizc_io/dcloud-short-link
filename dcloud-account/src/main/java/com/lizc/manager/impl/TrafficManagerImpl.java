package com.lizc.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lizc.enums.PluginTypeEnum;
import com.lizc.manager.TrafficManager;
import com.lizc.mapper.TrafficMapper;
import com.lizc.model.TrafficDO;
import com.lizc.util.CommonUtil;
import com.lizc.util.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class TrafficManagerImpl implements TrafficManager {

    @Autowired
    private TrafficMapper trafficMapper;

    @Override
    public int add(TrafficDO trafficDO) {
        return trafficMapper.insert(trafficDO);
    }

    @Override
    public IPage<TrafficDO> pageAvailable(int page, int size, long accountNo) {
        Page<TrafficDO> pageInfo = new Page<>(page, size);
        String today = TimeUtil.format(new Date(), "yyyy-MM-dd");
        Page<TrafficDO> trafficDOPage = trafficMapper.selectPage(pageInfo, new QueryWrapper<TrafficDO>()
                .eq("account_no", accountNo)
                .ge("expired_date", today)
                .orderByDesc("gmt_create"));
        return trafficDOPage;
    }



    @Override
    public TrafficDO findByIdAndAccountNo(Long trafficId, long accountNo) {
        TrafficDO trafficDO = trafficMapper.selectOne(new QueryWrapper<TrafficDO>()
                .eq("account_no", accountNo) //查询条件，先指定分片键，再指定其他查询条件
                .eq("id", trafficId));
        return trafficDO;
    }

    @Override
    public int deleteExpireTraffic() {
        int rows = trafficMapper.delete(new QueryWrapper<TrafficDO>().le("expired_date", new Date()));
        log.info("删除过期流量包行数: rows={}", rows);
        return rows;
    }

    /**
     * 查找未过期的流量包列表（不一定可用，可能超过次数）
     * 包含 付费+免费 流量包  免费流量包的out_trade_no 为free_init
     *  SELECT  * FROM traffic_0 WHERE (account_no = ? AND (expired_date >= ? OR out_trade_no = ?))
     * @param accountNo
     * @return
     */
    @Override
    public List<TrafficDO> selectAvailableTraffics(Long accountNo) {
        String today = TimeUtil.format(new Date(),"yyyy-MM-dd");
        QueryWrapper<TrafficDO> queryWrapper = new QueryWrapper<TrafficDO>();
        queryWrapper.eq("account_no", accountNo);
        queryWrapper.and(wrapper -> wrapper.ge("expired_date", today)
                .or()
                .eq("out_trade_no","free_init"));
        List<TrafficDO> trafficDOList = trafficMapper.selectList(queryWrapper);
        return trafficDOList;
    }


    /**
     * 增加流量包使用次数
     * @param accountNo
     * @param trafficId
     * @param usedTimes
     * @return
     */
    @Override
    public int addDayUsedTimes(Long accountNo, Long trafficId, Integer usedTimes) {
        int rows = trafficMapper.addDayUsedTimes(accountNo, trafficId, usedTimes);
        return rows;
    }

    @Override
    public int releaseUsedTimes(long accountNo, Long trafficId, Integer usedTimes, String useDateStr) {
        int rows = trafficMapper.releaseUsedTimes(accountNo, trafficId, usedTimes, useDateStr);
        return rows;
    }

    /**
     * 批量更新流量包，当天已使用次数为0
     * @param accountNo
     * @param unUpdatedTrafficIds
     * @return
     */
    @Override
    public int batchUpdateUsedTimes(Long accountNo, List<Long> unUpdatedTrafficIds) {
        int rows = trafficMapper.update(null, new UpdateWrapper<TrafficDO>()
                .eq("account_no", accountNo)
                .in("id", unUpdatedTrafficIds)
                .set("day_used", 0));
        return rows;
    }


}
