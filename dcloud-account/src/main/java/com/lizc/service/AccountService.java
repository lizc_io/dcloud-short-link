package com.lizc.service;

import com.lizc.model.AccountDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lizc.request.AccountLoginRequest;
import com.lizc.request.AccountRegisterRequest;
import com.lizc.util.JsonData;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-23
 */
public interface AccountService {

    JsonData regsiter(AccountRegisterRequest registerRequest);

    JsonData login(AccountLoginRequest loginRequest);
}
