package com.lizc.service;

import com.lizc.enums.SendCodeEnum;
import com.lizc.request.SendCodeRequest;
import com.lizc.util.JsonData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lizengcai
 * @version v1.0
 * @className NotifyService
 * @description TODO
 * @date 2024/3/24 2:52
 */
public interface NotifyService {

    void getCaptcha(HttpServletRequest request, HttpServletResponse response);

    boolean validateCaptcha(SendCodeRequest sendCodeRequest, HttpServletRequest request);

    JsonData sendCode(SendCodeEnum sendCodeEnum, String to);

    boolean checkCode(SendCodeEnum sendCodeEnum, String to, String code);
}
