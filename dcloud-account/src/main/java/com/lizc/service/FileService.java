package com.lizc.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author lizengcai
 * @version v1.0
 * @className FileService
 * @description TODO
 * @date 2024/3/24 18:00
 */
public interface FileService {
    
    String uploadUserImg(MultipartFile file);
}
