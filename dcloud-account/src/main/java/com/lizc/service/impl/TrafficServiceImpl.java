package com.lizc.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lizc.config.RabbitMQConfig;
import com.lizc.constant.RedisKey;
import com.lizc.controller.request.TrafficPageRequest;
import com.lizc.controller.request.UseTrafficRequest;
import com.lizc.enums.BizCodeEnum;
import com.lizc.enums.EventMessageType;
import com.lizc.enums.TaskStateEnum;
import com.lizc.exception.BizException;
import com.lizc.feign.ProductFeignService;
import com.lizc.feign.ShortLinkFeignService;
import com.lizc.interceptor.LoginInterceptor;
import com.lizc.manager.TrafficManager;
import com.lizc.manager.TrafficTaskManager;
import com.lizc.model.EventMessage;
import com.lizc.model.LoginUser;
import com.lizc.model.TrafficDO;
import com.lizc.model.TrafficTaskDO;
import com.lizc.service.TrafficService;
import com.lizc.util.JsonData;
import com.lizc.util.JsonUtil;
import com.lizc.util.TimeUtil;
import com.lizc.vo.ProductVO;
import com.lizc.vo.TrafficVO;
import com.lizc.vo.UseTrafficVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-23
 */
@Slf4j
@Service
public class TrafficServiceImpl implements TrafficService {

    @Autowired
    private TrafficManager trafficManager;

    @Autowired
    private ProductFeignService productFeignService;

    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;

    @Autowired
    private TrafficTaskManager trafficTaskManager;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitMQConfig rabbitMQConfig;

    @Autowired
    private ShortLinkFeignService shortLinkFeignService;

    @Override
    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
    public void handleTrafficMessage(EventMessage eventMessage) {
        String messageType = eventMessage.getEventMessageType();
        Long accountNo = eventMessage.getAccountNo();
        if(EventMessageType.PRODUCT_ORDER_PAY.name().equalsIgnoreCase(messageType)){
            //付费流量包
            //订单已经支付，新增流量
            String content = eventMessage.getContent();
            Map<String, Object> orderInfoMap = JsonUtil.json2Obj(content, Map.class);

            //还原订单商品信息
            String outTradeNo = (String)orderInfoMap.get("outTradeNo");
            Integer buyNum = (Integer)orderInfoMap.get("buyNum");
            String productStr = (String) orderInfoMap.get("product");
            ProductVO productVO = JsonUtil.json2Obj(productStr, ProductVO.class);
            log.info("商品信息: {}",productVO);

            //流量包有效期
            LocalDateTime expiredDateTime = LocalDateTime.now().plusDays(productVO.getValidDay());
            Date date = Date.from(expiredDateTime.atZone(ZoneId.systemDefault()).toInstant());

            //构建流量包对象
            TrafficDO trafficDO = TrafficDO.builder()
                    .accountNo(accountNo)
                    .dayLimit(productVO.getDayTimes() * buyNum)
                    .dayUsed(0)
                    .totalLimit(productVO.getTotalTimes())
                    .pluginType(productVO.getPluginType())
                    .level(productVO.getLevel())
                    .productId(productVO.getId())
                    .outTradeNo(outTradeNo)
                    .expiredDate(date).build();

            int rows = trafficManager.add(trafficDO);
            log.info("消费消息新增流量包:rows={}, trafficDO={}",rows,trafficDO);

            //新增流量包，应该删除这个key,重新计算放到redis
            String totalTrafficTimesKey = String.format(RedisKey.DAY_TOTAL_TRAFFIC, accountNo);
            redisTemplate.delete(totalTrafficTimesKey);

        }else if(EventMessageType.TRAFFIC_FREE_INIT.name().equalsIgnoreCase(messageType)){
            //新用户注册初始化免费流量包  使用唯一索引，避免重复消费
            Long productId = Long.valueOf(eventMessage.getBizId());

            JsonData jsonData = productFeignService.detail(productId);

            //RPC查询商品详情
            ProductVO productVO = jsonData.getData(new TypeReference<ProductVO>() {});

            //构建流量包对象
            TrafficDO trafficDO = TrafficDO.builder()
                    .accountNo(accountNo)
                    .dayLimit(productVO.getDayTimes())
                    .dayUsed(0)
                    .totalLimit(productVO.getTotalTimes())
                    .pluginType(productVO.getPluginType())
                    .level(productVO.getLevel())
                    .productId(productVO.getId())
                    .expiredDate(new Date())
                    .outTradeNo("free_init")
                    .build();

            trafficManager.add(trafficDO);

            //新增流量包，应该删除这个key,重新计算放到redis
            String totalTrafficTimesKey = String.format(RedisKey.DAY_TOTAL_TRAFFIC, accountNo);
            redisTemplate.delete(totalTrafficTimesKey);
        }else if(EventMessageType.TRAFFIC_USED.name().equalsIgnoreCase(messageType)){
            //流量包使用，检查是否成功使用
            //检查task是否存在
            //检查短链是否成功
            //如果不成功则恢复流量包，删除缓存key
            //删除task（也可以更新状态，定时删除也行）

            Long trafficTaskId = Long.valueOf(eventMessage.getBizId());
            TrafficTaskDO trafficTaskDO = trafficTaskManager.findByIdAndAccountNo(trafficTaskId, accountNo);

            //非空 且 是锁定状态
            if(trafficTaskDO!=null && trafficTaskDO.getLockState().equalsIgnoreCase(TaskStateEnum.LOCK.name())){
                JsonData jsonData = shortLinkFeignService.check(trafficTaskDO.getBizId());
                if(jsonData.getCode()!=0){
                    log.error("创建短链失败，流量包回滚");
                    String useDateStr = TimeUtil.format(trafficTaskDO.getGmtCreate(), "yyyy-MM-dd");
                    //回滚时指定日期，否则由于mq延迟消息可能更新到第二天的流量包
                    trafficManager.releaseUsedTimes(accountNo,trafficTaskDO.getTrafficId(),trafficTaskDO.getUseTimes(),useDateStr);

                    //流量包回滚，删除这个key,重新计算放到redis
                    String totalTrafficTimesKey = String.format(RedisKey.DAY_TOTAL_TRAFFIC, accountNo);
                    redisTemplate.delete(totalTrafficTimesKey);
                }
                //多种方式处理task, 不立刻删除，可以更新状态，然后定时删除也可以
                trafficTaskManager.deleteByIdAndAccountNo(trafficTaskId,accountNo);
            }

        }
    }

    @Override
    public Map<String, Object> pageAvailable(TrafficPageRequest request) {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        IPage<TrafficDO> trafficDOIPage = trafficManager.pageAvailable(request.getPage(), request.getSize(), loginUser.getAccountNo());
        
        //获取流量包列表
        List<TrafficDO> records = trafficDOIPage.getRecords();
        List<TrafficVO> trafficVOList = records.stream().map(obj -> beanProcess(obj)).collect(Collectors.toList());

        Map<String, Object> pageMap = new HashMap<>();
        pageMap.put("total_record", trafficDOIPage.getTotal());
        pageMap.put("total_page", trafficDOIPage.getPages());
        pageMap.put("current_data", trafficVOList);
        
        return pageMap;
    }

    @Override
    public TrafficVO detail(Long trafficId) {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        TrafficDO trafficDO = trafficManager.findByIdAndAccountNo(trafficId, loginUser.getAccountNo());
        TrafficVO trafficVO = beanProcess(trafficDO);
        return trafficVO;
    }

    public boolean deleteExpireTraffic(){
        trafficManager.deleteExpireTraffic();
        return false;
    }

    /**
     * 扣减流量包
     *
     * - 查询用户全部可用流量包
     * - 遍历用户可用流量包
     *   - 判断是否更新-用日期判断
     *     - 没更新的流量包后加入【待更新集合】中
     *       - 增加【今天剩余可用总次数】
     *     - 已经更新的判断是否超过当天使用次数
     *       - 如果没超过则增加【今天剩余可用总次数】
     *       - 超过则忽略
     * - 更新用户今日流量包相关数据
     * - 扣减使用的某个流量包使用次数
     * @param trafficRequest
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public JsonData reduce(UseTrafficRequest trafficRequest) {
        Long accountNo = trafficRequest.getAccountNo();

        //处理流量包，筛选未更新流量包、当前使用流量包
        UseTrafficVO useTrafficVO = processTrafficList(accountNo);
        log.info("今天可用总次数:{}, 当前使用的流量包:{}",useTrafficVO.getDayTotalLeftTimes(),useTrafficVO.getCurrentTrafficDO());

        //如果当前流量包为空，则没有可用流量包
        if(useTrafficVO.getCurrentTrafficDO() == null){
            return JsonData.buildResult(BizCodeEnum.TRAFFIC_REDUCE_FAIL);
        }

        log.info("待更新流量包列表：{}",useTrafficVO.getUnUpdatedTrafficIds());
        if(!useTrafficVO.getUnUpdatedTrafficIds().isEmpty()) {
            //更新今日流量包
            trafficManager.batchUpdateUsedTimes(accountNo, useTrafficVO.getUnUpdatedTrafficIds());
        }

        //一定要先更新当天流量包，再增加此次流量包扣减
        int rows = trafficManager.addDayUsedTimes(accountNo,  useTrafficVO.getCurrentTrafficDO().getId(),1);

        //保存Task
        TrafficTaskDO trafficTaskDO = TrafficTaskDO.builder().accountNo(accountNo)
                .bizId(trafficRequest.getBizId())
                .useTimes(1).trafficId(useTrafficVO.getCurrentTrafficDO().getId())
                .lockState(TaskStateEnum.LOCK.name()).build();
        trafficTaskManager.add(trafficTaskDO);

        if(rows !=1){
            throw new BizException(BizCodeEnum.TRAFFIC_REDUCE_FAIL);
        }

        //往Redis设置流量包总次数，短链服务那边递减次数，如果有新增流量包，则删除这个key TODO
        //获取当天剩余的时间
        long leftSeconds = TimeUtil.getRemainSecondsOneDay(new Date());
        String totalKey = String.format(RedisKey.DAY_TOTAL_TRAFFIC, accountNo);
        //总次数 减少1 是减去此次的流量次数
        redisTemplate.opsForValue().set(totalKey, useTrafficVO.getDayTotalLeftTimes() - 1, leftSeconds, TimeUnit.SECONDS);


        EventMessage usedTrafficEventMessage = EventMessage.builder()
                .accountNo(accountNo)
                .bizId(trafficTaskDO.getId() + "")
                .eventMessageType(EventMessageType.TRAFFIC_USED.name())
                .build();
        //发送延迟信息,用于异常回滚，数据最终一致性
        rabbitTemplate.convertAndSend(rabbitMQConfig.getTrafficEventExchange(),
                rabbitMQConfig.getTrafficReleaseDelayRoutingKey(), usedTrafficEventMessage);

        return JsonData.buildSuccess();
    }

    /**
     * 处理流量包，筛选未更新流量包、当前使用流量包
     * @param accountNo
     */
    private  UseTrafficVO processTrafficList(Long accountNo){
        //全部流量包
        List<TrafficDO> list = trafficManager.selectAvailableTraffics(accountNo);
        if (list == null || list.isEmpty()) {
            throw new BizException(BizCodeEnum.TRAFFIC_EXCEPTION);
        }
        //天剩余可用总次数 = 总次数-已用  (所有流量包的剩余总次数)
        int dayTotalLeftTimes = 0;

        //当前使用流量包
        TrafficDO currentTrafficDO = null;

        //没过期，且今天没更新的流量包
        List<Long> unUpdatedTrafficIds = new ArrayList<>();

        //今天日期
        String todayStr = TimeUtil.format(new Date(),"yyyy-MM-dd");

        for(TrafficDO trafficDO : list){
            //判断是否更新，用日期判断，不能用时间
            String trafficUpdateDate = TimeUtil.format(trafficDO.getGmtModified(),"yyyy-MM-dd");
            if(todayStr.equalsIgnoreCase(trafficUpdateDate)){
                //已经更新  剩余可用 = 天总次数-已用次数
                int dayLeftTimes = trafficDO.getDayLimit()-trafficDO.getDayUsed();
                dayTotalLeftTimes = dayTotalLeftTimes + dayLeftTimes;

                //选取 当次流量包 要可用
                if(dayLeftTimes>0 && currentTrafficDO == null){
                    currentTrafficDO = trafficDO;
                }
            }else {
                //未更新
                dayTotalLeftTimes = dayTotalLeftTimes + trafficDO.getDayLimit();
                //记录未更新流量包  剩余可用 = 天总次数
                unUpdatedTrafficIds.add(trafficDO.getId());

                //选取 当次流量包
                if(currentTrafficDO == null){
                    currentTrafficDO = trafficDO;
                }
            }
        }

        UseTrafficVO useTrafficVO = new UseTrafficVO(dayTotalLeftTimes, currentTrafficDO, unUpdatedTrafficIds);

        return useTrafficVO;
    }


    private TrafficVO beanProcess(TrafficDO trafficDO) {
        TrafficVO trafficVO = new TrafficVO();
        BeanUtils.copyProperties(trafficDO, trafficVO);
        return trafficVO;
    }
}
