package com.lizc.service.impl;

import com.google.code.kaptcha.Producer;
import com.lizc.component.SmsComponent;
import com.lizc.config.SmsConfig;
import com.lizc.enums.BizCodeEnum;
import com.lizc.enums.SendCodeEnum;
import com.lizc.request.SendCodeRequest;
import com.lizc.service.NotifyService;
import com.lizc.util.CheckUtil;
import com.lizc.util.CommonUtil;
import com.lizc.util.JsonData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author lizengcai
 * @version v1.0
 * @className NotifyServiceImpl
 * @description TODO
 * @date 2024/3/24 2:52
 */
@Slf4j
@Component
public class NotifyServiceImpl implements NotifyService {

    /**
     * 验证码缓存key，第一个是类型,第二个是唯一标识比如手机号或者邮箱
     */
    public static final String CHECK_CODE_KEY = "code:%s:%s";


    /**
     * 10分钟有效
     */
    private static final int CODE_EXPIRED = 60*1000*10;
    /**
     *临时使用10分钟有效，方便测试
     */
    private static final long CAPTCHA_CODE_EXPIRED = 60 * 1000 * 10;

    @Autowired
    private SmsComponent smsComponent;

    @Autowired
    private Producer captchaProducer;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private SmsConfig smsConfig;


    @Override
    public void getCaptcha(HttpServletRequest request, HttpServletResponse response) {
        String captchaText = captchaProducer.createText();
        log.info("验证码内容:{}", captchaText);

        //存储Redis，配置过期时间
        redisTemplate.opsForValue().set(getCaptchaKey(request), captchaText, CAPTCHA_CODE_EXPIRED, TimeUnit.MILLISECONDS);

        //响应前端
        BufferedImage bufferedImage = captchaProducer.createImage(captchaText);
        try(ServletOutputStream outputStream = response.getOutputStream()){
            ImageIO.write(bufferedImage, "jpg", outputStream);
            outputStream.flush();
        }catch (IOException e){
            log.error("获取流出错:{}", e.getMessage());
        }
    }


    /**
     * @author lizengcai
     * @description 校验图形验证码
     * @date 2024/3/24 17:11
     * @param sendCodeRequest
     * @param request
     * @return boolean
     */
    public boolean validateCaptcha(SendCodeRequest sendCodeRequest, HttpServletRequest request) {
        String captchaCacheKey = this.getCaptchaKey(request);
        String cacheCaptcha = redisTemplate.opsForValue().get(captchaCacheKey);
        String captcha = sendCodeRequest.getCaptcha();
        if( captcha != null && cacheCaptcha !=null && cacheCaptcha.equalsIgnoreCase(captcha)){
            //删除图形验证码，删除失败也会自动过期
            redisTemplate.delete(captchaCacheKey);
            return true;
        }else{
            return false;
        }
    }


    @Override
    public JsonData sendCode(SendCodeEnum sendCodeEnum, String to) {
        String cacheKey = String.format(CHECK_CODE_KEY, sendCodeEnum.name(), to);
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);

        //校验短信验证码是否在时间内重新获取
        //如果不为空，再判断是否是60秒内重复发送 0122_232131321314132
        if(StringUtils.isNotBlank(cacheValue)){
            long ttl = Long.parseLong(cacheValue.split("_")[1]);
            long leftTime = CommonUtil.getCurrentTimestamp() - ttl;
            //当前时间戳-验证码发送的时间戳，如果小于60秒，则不给重复发送
            if(leftTime <= (1000*60)){
                log.info("重复发送短信验证码，时间间隔:{}秒", leftTime/1000);
                return JsonData.buildResult(BizCodeEnum.CODE_LIMITED);
            }
        }

        //校验通过，重新发送短信验证码
        String code = CommonUtil.getRandomCode(6);
        String value = code + "_" + CommonUtil.getCurrentTimestamp();
        redisTemplate.opsForValue().set(cacheKey, value, CODE_EXPIRED, TimeUnit.MILLISECONDS);
        if(CheckUtil.isEmail(to)){
            //邮箱验证码
        }else if(CheckUtil.isPhone(to)){
            //短信验证码
            smsComponent.send(to, smsConfig.getTemplateId(), code);
        }
        return JsonData.buildSuccess();
    }

    /**
     * 验证码校验逻辑
     */
    @Override
    public boolean checkCode(SendCodeEnum sendCodeEnum, String to, String code) {
        String cacheKey = String.format(CHECK_CODE_KEY, sendCodeEnum.name(), to);
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);
        if(StringUtils.isNotBlank(cacheValue)){
            String cacheCode = cacheValue.split("_")[0];
            if(cacheCode.equalsIgnoreCase(code)){
                redisTemplate.delete(cacheKey);
                return true;
            }
        }
        return false;
    }

    private String getCaptchaKey(HttpServletRequest request){
        String ipAddr = CommonUtil.getIpAddr(request);
        String userAgent = request.getHeader("User-Agent");
        String key = "account-service:captcha:" + CommonUtil.MD5(ipAddr + userAgent);
        log.info("验证码key:{}", key);
        return key;
    }
}
