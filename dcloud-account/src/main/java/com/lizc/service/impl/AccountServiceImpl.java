package com.lizc.service.impl;

import com.lizc.config.RabbitMQConfig;
import com.lizc.enums.AuthTypeEnum;
import com.lizc.enums.BizCodeEnum;
import com.lizc.enums.EventMessageType;
import com.lizc.enums.SendCodeEnum;
import com.lizc.manager.AccountManager;
import com.lizc.model.AccountDO;
import com.lizc.model.EventMessage;
import com.lizc.model.LoginUser;
import com.lizc.request.AccountLoginRequest;
import com.lizc.request.AccountRegisterRequest;
import com.lizc.service.AccountService;
import com.lizc.service.NotifyService;
import com.lizc.util.CommonUtil;
import com.lizc.util.IDUtil;
import com.lizc.util.JWTUtil;
import com.lizc.util.JsonData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-23
 */
@Slf4j
@Service
public class AccountServiceImpl implements AccountService {

    /**
     * 免费流量包ID
     */
    private static final Long FREE_TRAFFIC_PRODUCT_ID = 1L;

    @Autowired
    private NotifyService notifyService;
    @Autowired
    private AccountManager accountManager;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitMQConfig rabbitMQConfig;

    /**
     * 伪代码：
     * 手机验证码验证
     * 密码加密（TODO）
     * 账号唯一性检查(TODO)
     * 插入数据库
     * 新注册用户福利发放(TODO) ：新注册用户可以有免费的短链次数
     */
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public JsonData regsiter(AccountRegisterRequest registerRequest) {
        boolean checkCode = false;
        //手机验证码校验
        if(StringUtils.isNotBlank(registerRequest.getPhone())){
            checkCode = notifyService.checkCode(SendCodeEnum.USER_REGISTER, registerRequest.getPhone(), registerRequest.getCode());
        }
//        if(!checkCode){
//            return JsonData.buildResult(BizCodeEnum.CODE_ERROR);
//        }

        AccountDO accountDO = new AccountDO();
        BeanUtils.copyProperties(registerRequest, accountDO);

        //认证级别
        accountDO.setAuth(AuthTypeEnum.DEFAULT.name());

        //生成唯一的账号 TODO 后面会改
        accountDO.setAccountNo(Long.parseLong(IDUtil.geneSnowFlakeID().toString()));

        //设置密码，密钥
        accountDO.setSecret("$1$"+ CommonUtil.getStringNumRandom(8));
        String cryptPwd = Md5Crypt.md5Crypt(accountDO.getPwd().getBytes(), accountDO.getSecret());
        accountDO.setPwd(cryptPwd);

        int rows = accountManager.insert(accountDO);
        log.info("rows:{},注册成功:{}", rows, accountDO);

        userRegisterInitTask(accountDO);

        return JsonData.buildSuccess();
    }

    /**
     * 通过phone找数据库记录
     * 获取盐，和当前传递的密码就行加密后匹配
     * 生成token令牌
     */
    @Override
    public JsonData login(AccountLoginRequest loginRequest) {
        List<AccountDO> accountDOList = accountManager.findByPhone(loginRequest.getPhone());
        if(accountDOList != null && accountDOList.size() == 1){
           AccountDO accountDO = accountDOList.get(0);
           String md5Crypt = Md5Crypt.md5Crypt(loginRequest.getPwd().getBytes(), accountDO.getSecret());
           if(md5Crypt.equalsIgnoreCase(accountDO.getPwd())){
               LoginUser loginUser = LoginUser.builder().build();
               BeanUtils.copyProperties(accountDO, loginUser);
               //生成token
               String token = JWTUtil.geneJsonWebToken(loginUser);
               return JsonData.buildSuccess(token);
           }else{
               return JsonData.buildResult(BizCodeEnum.ACCOUNT_PWD_ERROR);
           }
        }else{
            return JsonData.buildResult(BizCodeEnum.ACCOUNT_UNREGISTER);
        }
    }


    /**
     * 用户注册，初始化福利信息
     * 注册成功后在发送
     *
     * (消费前查询下是否有这个用户,非必要)
     * @param accountDO
     */
    private void userRegisterInitTask(AccountDO accountDO) {
        EventMessage freeTrafficEventMessage = EventMessage.builder()
                .messageId(IDUtil.geneSnowFlakeID().toString())
                .accountNo(accountDO.getAccountNo())
                .eventMessageType(EventMessageType.TRAFFIC_FREE_INIT.name())
                //业务id, 免费流量包使用固定的id
                .bizId(FREE_TRAFFIC_PRODUCT_ID.toString())
                .build();

        rabbitTemplate.convertAndSend(rabbitMQConfig.getTrafficEventExchange(),
                rabbitMQConfig.getTrafficFreeInitRoutingKey(), freeTrafficEventMessage);

    }

}
