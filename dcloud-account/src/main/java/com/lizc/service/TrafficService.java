package com.lizc.service;

import com.lizc.controller.request.TrafficPageRequest;
import com.lizc.controller.request.UseTrafficRequest;
import com.lizc.model.EventMessage;
import com.lizc.model.TrafficDO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lizc.util.JsonData;
import com.lizc.vo.TrafficVO;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-23
 */
public interface TrafficService{

    void handleTrafficMessage(EventMessage eventMessage);

    Map<String, Object> pageAvailable(TrafficPageRequest request);

    TrafficVO detail(Long trafficId);

    /**
     * 删除过期流量包
     * @return
     */
    boolean deleteExpireTraffic();

    JsonData reduce(UseTrafficRequest useTrafficRequest);
}
