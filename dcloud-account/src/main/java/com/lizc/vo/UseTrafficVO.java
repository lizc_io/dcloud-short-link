package com.lizc.vo;

import com.lizc.model.TrafficDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UseTrafficVO {

    /**
     * 天剩余可用总次数 = 总次数-已用
     */
    private  Integer dayTotalLeftTimes;

    /**
     * 当前使用流量包
     */
    private TrafficDO currentTrafficDO ;

    /**
     * 没过期，且今天没更新的流量包
     */
    private List<Long> unUpdatedTrafficIds = new ArrayList<>();

}