package com.lizc.listener;


import com.lizc.enums.BizCodeEnum;
import com.lizc.exception.BizException;
import com.lizc.model.EventMessage;
import com.lizc.service.TrafficService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
@RabbitListener(queuesToDeclare = {
        @Queue("order.traffic.queue"),  //订单发放付费流量包队列
        @Queue("traffic.free_init.queue"),  //免费流量包队列
        @Queue("traffic.release.queue"),  //免费流量包队列
})
public class TrafficMQListener {

    @Autowired
    private TrafficService trafficService;

    @RabbitHandler
    public void trafficHandler(EventMessage eventMessage, Message message, Channel channel) throws IOException {
        log.info("监听到消息TrafficMQListener message消息内容:{}", message);
        try {
            trafficService.handleTrafficMessage(eventMessage);
        } catch (Exception e) {
            e.getStackTrace();
            //处理业务异常，还有进行其他操作，比如记录失败原因
            log.error("消费失败:{}", eventMessage);
            throw new BizException(BizCodeEnum.MQ_CONSUME_EXCEPTION);
        }
        log.info("消费成功:{}", eventMessage);
    }
}
