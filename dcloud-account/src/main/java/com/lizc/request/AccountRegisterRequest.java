package com.lizc.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author lizengcai
 * @version v1.0
 * @className AccountRegisterRequest
 * @description TODO
 * @date 2024/3/24 21:27
 */
@Data
public class AccountRegisterRequest {
    /**
     * 头像
     */
    private String headImg;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 邮箱
     */
    private String mail;

    /**
     * 用户名
     */
    private String username;


    /**
     * 短信验证码
     */
    private String code;
}
