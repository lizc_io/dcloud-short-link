package com.lizc.request;

import lombok.Data;

/**
 * @author lizengcai
 * @version v1.0
 * @className AccountLoginRequest
 * @description TODO
 * @date 2024/3/25 10:07
 */
@Data
public class AccountLoginRequest {

    private String phone;

    private String pwd;
}
