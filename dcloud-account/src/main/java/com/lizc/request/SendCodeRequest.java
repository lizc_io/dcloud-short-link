package com.lizc.request;

import lombok.Data;

/**
 * @author lizengcai
 * @version v1.0
 * @className SendCodeRequest
 * @description TODO
 * @date 2024/3/24 17:03
 */
@Data
public class SendCodeRequest {
    /**
    * 图形验证码
    */
    private String captcha;

    /**
     * 发送对方的手机号/邮箱
     */
    private String to;
}
