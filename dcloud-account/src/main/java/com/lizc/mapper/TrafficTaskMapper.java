package com.lizc.mapper;

import com.lizc.model.TrafficTaskDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-23
 */
public interface TrafficTaskMapper extends BaseMapper<TrafficTaskDO> {

}
