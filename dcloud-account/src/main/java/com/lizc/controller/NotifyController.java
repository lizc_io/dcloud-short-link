package com.lizc.controller;

import com.lizc.enums.BizCodeEnum;
import com.lizc.enums.SendCodeEnum;
import com.lizc.request.SendCodeRequest;
import com.lizc.service.NotifyService;
import com.lizc.util.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lizengcai
 * @version v1.0
 * @className NotifyController
 * @description TODO
 * @date 2024/3/24 2:52
 */

@RestController
@RequestMapping("/api/notify/v1")
public class NotifyController {

    @Autowired
    private NotifyService notifyService;

    @GetMapping("captcha")
    public void getCaptcha(HttpServletRequest request, HttpServletResponse response){
        notifyService.getCaptcha(request, response);
    }

    /**
     * 发送短信验证码
     * @return
     */
    @PostMapping("send_code")
    public JsonData sendCode(@RequestBody SendCodeRequest sendCodeRequest, HttpServletRequest request){
        //校验图形验证码
        boolean isPass = notifyService.validateCaptcha(sendCodeRequest, request);
        if(!isPass){
            return JsonData.buildResult(BizCodeEnum.CODE_CAPTCHA_ERROR);
        }
        JsonData jsonData = notifyService.sendCode(SendCodeEnum.USER_REGISTER, sendCodeRequest.getTo());
        return jsonData;
    }

}
