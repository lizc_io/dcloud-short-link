package com.lizc.controller;


import com.lizc.controller.request.TrafficPageRequest;
import com.lizc.controller.request.UseTrafficRequest;
import com.lizc.service.TrafficService;
import com.lizc.util.JsonData;
import com.lizc.vo.TrafficVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-23
 */
@RestController
@RequestMapping("/api/traffic/v1")
public class TrafficController {

    @Autowired
    private TrafficService trafficService;

    @Value("${rpc.token}")
    private String rpcToken;

    /**
     * 只能查看可用的流量包，不可用的需要去归档数据查
     *
     */
    @PostMapping("page")
    public JsonData pageAvailable(@RequestBody TrafficPageRequest request) {
        Map<String, Object> pageResult = trafficService.pageAvailable(request);
        return JsonData.buildSuccess(pageResult);
    }

    /**
     * 流量包详情
     *
     * @return
     */
    @GetMapping("/detail/{trafficId}")
    public JsonData detail(@PathVariable("trafficId") Long trafficId) {
        TrafficVO trafficVO = trafficService.detail(trafficId);
        return JsonData.buildSuccess(trafficVO);
    }

    /**
     * 使用流量包API
     */
    @PostMapping("reduce")
    public JsonData useTraffic(@RequestBody UseTrafficRequest useTrafficRequest, HttpServletRequest request){
        String requestToken = request.getHeader("rpc-token");
        if(rpcToken.equalsIgnoreCase(requestToken)){
            //具体使用流量包逻辑
            JsonData jsonData = trafficService.reduce(useTrafficRequest);
            return jsonData;
        }else{
            return JsonData.buildError("非法访问");
        }
    }
}

