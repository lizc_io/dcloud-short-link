package com.lizc.controller.request;

import lombok.Data;

@Data
public class TrafficPageRequest {

    private int page;

    private int size;
}
