package com.lizc.controller;


import com.lizc.enums.BizCodeEnum;
import com.lizc.request.AccountLoginRequest;
import com.lizc.request.AccountRegisterRequest;
import com.lizc.service.AccountService;
import com.lizc.service.FileService;
import com.lizc.util.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-23
 */
@RestController
@RequestMapping("/api/account/v1")
public class AccountController {

    @Autowired
    private FileService fileService;

    @Autowired
    private AccountService accountService;


    /**
     * 文件上传，默认最大1M
     */
    @PostMapping("upload")
    public JsonData uploadUserImg(@RequestPart("file") MultipartFile file){
        String result = fileService.uploadUserImg(file);
        if(result == null){
            return JsonData.buildResult(BizCodeEnum.FILE_UPLOAD_USER_IMG_FAIL);
        }else{
            return JsonData.buildSuccess(result);
        }
    }

    @PostMapping("register")
    public JsonData register(@RequestBody AccountRegisterRequest registerRequest){
        JsonData jsonData = accountService.regsiter(registerRequest);
        return jsonData;
    }


    @PostMapping("login")
    public JsonData login(@RequestBody AccountLoginRequest loginRequest){
        JsonData jsonData = accountService.login(loginRequest);
        return jsonData;
    }

}

