package com.lizc.biz;


import com.lizc.AccountApplication;
import com.lizc.component.SmsComponent;
import com.lizc.config.SmsConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author lizengcai
 * @version v1.0
 * @className SmsTest
 * @description TODO
 * @date 2024/3/24 0:34
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AccountApplication.class)
@Slf4j
public class SmsTest {

    @Autowired
    private SmsComponent smsComponent;

    @Autowired
    private SmsConfig smsConfig;


    @Test
    public void testSendSms(){
        smsComponent.send("18334705057", smsConfig.getTemplateId(), "123456");
    }


}
