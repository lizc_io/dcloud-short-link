package com.lizc.config;

import com.lizc.enums.BizCodeEnum;
import com.lizc.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author lizengcai
 * @version v1.0
 * @className SnowFlakeWordIdConfig
 * @description TODO
 * @date 2024/3/27 19:58
 */
@Slf4j
@Configuration
public class SnowFlakeWordIdConfig {

    /**
     * 动态指定sharding jdbc 的雪花算法中的属性work.id属性
     * 通过调用System.setProperty()的方式实现,可用容器的 id 或者机器标识位
     * workId最大值 1L << 100，就是1024，即 0<= workId < 1024
     * {@link SnowflakeShardingKeyGenerator#getWorkerId()}
     *
     */
    static {
        try {
            //获取当前机器IP进行哈希，对1024取模，这里是因为雪花算法的工作机器id占10个bit也就是支持1024个节点
            InetAddress ip4 = Inet4Address.getLocalHost();
            String addressIp = ip4.getHostAddress();
            String workId = Math.abs(addressIp.hashCode()) % 1024+"";
            //放在系统的环境变量里面
            System.setProperty("workId", workId);
            log.info("workId:{}",workId);
        } catch (UnknownHostException e) {
            throw new BizException(BizCodeEnum.OPS_NETWORK_ADDRESS_ERROR);
        }
    }
}