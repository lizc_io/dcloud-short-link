package com.lizc.exception;

/**
 * @author lizengcai
 * @version v1.0
 * @className BizException
 * @description TODO
 * @date 2024/3/23 20:36
 */

import com.lizc.enums.BizCodeEnum;
import lombok.Data;

/**
 * 全局异常处理
 */
@Data
public class BizException extends RuntimeException {

    private Integer code;
    private String msg;

    public BizException(Integer code, String message) {
        super(message);
        this.code = code;
        this.msg = message;
    }

    public BizException(BizCodeEnum bizCodeEnum) {
        super(bizCodeEnum.getMessage());
        this.code = bizCodeEnum.getCode();
        this.msg = bizCodeEnum.getMessage();
    }

}