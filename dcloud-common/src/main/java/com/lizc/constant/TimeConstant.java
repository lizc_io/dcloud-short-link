package com.lizc.constant;

/**
 * @author lizengcai
 * @version v1.0
 * @className TimeConstant
 * @description TODO
 * @date 2024/4/6 1:01
 */
public class TimeConstant {

    /**
     * 默认30分钟超时未支付，单位：毫秒
     */
    public static final long ORDER_PAY_TIMEOUT_MILLS = 1000 * 60 * 30;
}
