package com.lizc.constant;

/**
 * @author lizengcai
 * @version v1.0
 * @className RedisKey
 * @description TODO
 * @date 2024/4/6 1:59
 */
public class RedisKey {

    public static final String CHECK_CODE_KEY = "code:%s:%s";

    /**
     * 提交订单令牌的缓存key
     */
    public static final String SUBMIT_ORDER_TOKEN_KEY = "order:submit:%s:%s";

    /**
     * 1天总的流量包
     */
    public static final String DAY_TOTAL_TRAFFIC = "lock:traffic:day_total:%s";
}
