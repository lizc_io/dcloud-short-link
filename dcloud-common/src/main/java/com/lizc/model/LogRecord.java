package com.lizc.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 日志对象
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class LogRecord {

    /**
     * 客户端ip
     */
    private String ip;

    /**
     * 产生时间戳
     */
    private Long ts;

    /**
     * 日志事件类型
     */
    private String event;

    /**
     * udid, 设备的唯一标识，全称 uniqueDeviceIdentifier
     */
    private String udid;

    /**
     * 业务id
     */
    private String bizId;

    /**
     * 日志内容
     */
    private Object data;
}
