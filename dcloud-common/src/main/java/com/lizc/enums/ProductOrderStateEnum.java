package com.lizc.enums;

/**
 * @author lizengcai
 * @version v1.0
 * @className ProductOrderStateEnum
 * @description TODO
 * @date 2024/4/6 0:52
 */
public enum ProductOrderStateEnum {

    /**
     * 未支付
     */
    NEW,

    /**
     * 已经支付
     */
    PAY,

    /**
     * 超时取消支付
     */
    CANCEL,
}
