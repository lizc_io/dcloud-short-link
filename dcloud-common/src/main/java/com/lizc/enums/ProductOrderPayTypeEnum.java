package com.lizc.enums;

/**
 * @author lizengcai
 * @version v1.0
 * @className ProductOrderPayTypeEnum
 * @description TODO
 * @date 2024/4/6 0:19
 */
public enum ProductOrderPayTypeEnum {

    WECHAT_PAY,

    ALI_PAY,

    BANK_PAY;
}
