package com.lizc.enums;

/**
 * @author lizengcai
 * @version v1.0
 * @className BillTypeEnum
 * @description TODO
 * @date 2024/4/6 0:49
 */
public enum BillTypeEnum {

    /**
     * 不用发票
     */
    NO_BILL,

    /**
     * 纸质发票
     */
    PAPER_BILL,

    /**
     * 电子发票
     */
    ELE_BILL
}
