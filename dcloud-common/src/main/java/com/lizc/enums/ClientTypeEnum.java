package com.lizc.enums;

/**
 * @author lizengcai
 * @version v1.0
 * @className ClientTypeEnum
 * @description TODO
 * @date 2024/4/6 0:19
 */
public enum ClientTypeEnum {

    /**
     * app支付
     */
    APP,

    /**
     * 网页
     */
    PC,

    /**
     * 移动端H5
     */
    H5;
}
