package com.lizc.enums;


/**
 * 插件类型
 */
public enum PluginTypeEnum {

    SHORT_LINK,

    QRCODE;
}
