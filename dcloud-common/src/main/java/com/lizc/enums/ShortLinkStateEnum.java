package com.lizc.enums;

public enum ShortLinkStateEnum {

    /**
     * 锁定
     */
    LOCK,

    /**
     * 可用
     */
    ACTIVE;

}