package com.lizc.enums;

public enum EventMessageType {

    SHORT_LINK_ADD,


    /**
     * 创建短链 C端
     */
    SHORT_LINK_ADD_LINK,


    /**
     * 创建短链 B端
     */
    SHORT_LINK_ADD_MAPPING,


    SHORT_LINK_DEL,

    /**
     * 删除短链 C端
     */
    SHORT_LINK_DEL_LINK,


    /**
     * 删除短链 B端
     */
    SHORT_LINK_DEL_MAPPING,

    SHORT_LINK_UPDATE,

    /**
     * 更新短链 C端
     */
    SHORT_LINK_UPDATE_LINK,

    /**
     * 更新短链 B端
     */
    SHORT_LINK_UPDATE_MAPPING,


    /**
     * 新建商品订单
     */
    PRODUCT_ORDER_NEW,


    /**
     * 订单支付
     */
    PRODUCT_ORDER_PAY,

    /**
     * 免费发放流量包
     */
    TRAFFIC_FREE_INIT,

    /**
     * 使用流量包
     */
    TRAFFIC_USED;
}
