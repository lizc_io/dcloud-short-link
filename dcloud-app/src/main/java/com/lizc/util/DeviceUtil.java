package com.lizc.util;

import com.lizc.model.DeviceInfoDO;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.lang3.StringUtils;

import java.security.MessageDigest;
import java.util.Map;

//设备工具类
public class DeviceUtil {

    /**
     * 生成web设备唯一ID
     * @param map
     * @return
     */
    public static String geneWebUniqueDeviceId(Map<String,String> map){
        String deviceId = MD5(map.toString());
        return deviceId;
    }

    /**
     * MD5加密
     *
     * @param data
     * @return
     */
    public static String MD5(String data) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(data.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (byte item : array) {
                sb.append(Integer.toHexString((item & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString().toUpperCase();
        } catch (Exception exception) {
        }
        return null;

    }

    /**
     * 浏览器名称
     * @param userAgent
     * @return  Firefox、Chrome
     */
    public static String getBrowserName(String userAgent) {
        Browser browser =  getBrowser(userAgent);
        String browserGroup = browser.getGroup().getName();
        return browserGroup;
    }

    /**
     * 获取deviceType
     * @param userAgent
     *
     * @return  MOBILE、COMPUTER
     */
    public static String getDeviceType(String userAgent) {
        OperatingSystem operatingSystem =  getOperatingSystem(userAgent);
        String deviceType = operatingSystem.getDeviceType().toString();
        return deviceType;
    }



    /**
     * 获取os：Windows/ios/Android
     * @param userAgent
     * @return
     */
    public static String getOS(String userAgent) {
        OperatingSystem operatingSystem =  getOperatingSystem(userAgent);
        String os = operatingSystem.getGroup().getName();
        return os;
    }


    /**
     * 获取device的生产厂家
     *
     * @param userAgent
     * @return GOOGLE、APPLE
     */
    public static String getDeviceManufacturer(String userAgent) {
        OperatingSystem operatingSystem =  getOperatingSystem(userAgent);

        String deviceManufacturer = operatingSystem.getManufacturer().toString();
        return deviceManufacturer;
    }




    /**
     * 操作系统版本
     * @param userAgent
     * @return Android 1.x、Intel Mac OS X 10.15
     */
    public static String getOSVersion(String userAgent) {
        String osVersion = "";
        if(StringUtils.isBlank(userAgent)) {
            return osVersion;
        }
        String[] strArr = userAgent.substring(userAgent.indexOf("(")+1,
                userAgent.indexOf(")")).split(";");
        if(null == strArr || strArr.length == 0) {
            return osVersion;
        }

        osVersion = strArr[1];
        return osVersion;
    }



    /**
     * 获取浏览器对象
     * @param agent
     * @return
     */
    private static Browser getBrowser(String agent) {
        UserAgent userAgent = UserAgent.parseUserAgentString(agent);
        Browser browser = userAgent.getBrowser();
        return browser;
    }


    /**
     * 获取操作系统对象
     * @param userAgent
     * @return
     */
    private static OperatingSystem getOperatingSystem(String userAgent) {
        UserAgent agent = UserAgent.parseUserAgentString(userAgent);
        OperatingSystem operatingSystem = agent.getOperatingSystem();
        return operatingSystem;
    }

    /**
     * 解析DeviceInfoDO对象
     */
    public static DeviceInfoDO getDeviceInfo(String agent){
        UserAgent userAgent = UserAgent.parseUserAgentString(agent);
        Browser browser = userAgent.getBrowser();
        OperatingSystem operatingSystem = userAgent.getOperatingSystem();

        String browserName = browser.getGroup().getName();
        String os = operatingSystem.getGroup().getName();
        String manufacture = operatingSystem.getManufacturer().toString();
        String deviceType = operatingSystem.getDeviceType().toString();

        DeviceInfoDO deviceInfoDO = DeviceInfoDO.builder()
                .browserName(browserName)
                .deviceManufacturer(manufacture)
                .deviceType(deviceType)
                .os(os)
                .osVersion(getOSVersion(agent))
                .build();
        return deviceInfoDO;
    }

}