package com.lizc.func;

import com.alibaba.fastjson.JSONObject;
import com.lizc.util.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.configuration.Configuration;


@Slf4j
public class VisitorMapFunction extends RichMapFunction<JSONObject, String> {

    /**
     * 记录用户的udid访问状态
     */
    private ValueState<String> newDayVisitorState;


    /**
     * map函数初始化的时候调用一次，初始化valueState
     * @param parameters
     * @throws Exception
     */
    @Override
    public void open(Configuration parameters) throws Exception {
        //对状态做初始化: 变量名,存储类型
        newDayVisitorState = getRuntimeContext().getState(new ValueStateDescriptor<String>("newDayVisitorState", String.class));
    }

    /**
     * 新老访客和uv 对比，如果都是天维度的话就一样
     *  新老访客可以是指1天内、1个月内维度
     *  uv可以是指 月独立、日独立访客
     *
     * @param value
     * @return
     * @throws Exception
     */
    @Override
    public String map(JSONObject value) throws Exception {
        //获取之前是否有访问日期
        String beforeDateState = newDayVisitorState.value();

        //获取当前访问时间戳
        Long ts = value.getLong("ts");
        String currentDateStr = TimeUtil.format(ts);

        //如果状态不为空，并且状态日期和当前日期不相等，则是老访客
        if(StringUtils.isNotBlank(beforeDateState)){
            //判断是否为同一天数据，一样则是老访客
            if (beforeDateState.equals(currentDateStr)) {
                value.put("is_new", 0);
                System.out.println("老访客 "+currentDateStr);
            } else {
                //不一样是则是新访客，更新访问日期
                value.put("is_new", 1);
                newDayVisitorState.update(currentDateStr);
                log.info("新访客: {}", currentDateStr);
            }
        }else{
            //如果状态为空，则之前没访问过，是新访客，标记1，老访客，标记0
            value.put("is_new", 1);
            newDayVisitorState.update(currentDateStr);
            log.info("新访客: {}", currentDateStr);
        }

        //处理完之后，要返回到dwd层
        return value.toJSONString();
    }
}
