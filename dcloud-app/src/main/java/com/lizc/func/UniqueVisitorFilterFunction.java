package com.lizc.func;

import com.alibaba.fastjson.JSONObject;
import com.lizc.util.TimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.RichFilterFunction;
import org.apache.flink.api.common.state.StateTtlConfig;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.time.Time;
import org.apache.flink.configuration.Configuration;


@Slf4j
public class UniqueVisitorFilterFunction extends RichFilterFunction<JSONObject> {


    //定义状态，存储访问时间
    private ValueState<String> lastVisitDateState = null;

    @Override
    public void open(Configuration parameters) throws Exception {
        //初始化状态
        ValueStateDescriptor<String> lastVisitDateStateDes = new ValueStateDescriptor<>("lastVisitDateState", String.class);
        //统计日活DAU，状态数据当天有效
        StateTtlConfig stateTtlConfig = StateTtlConfig.newBuilder(Time.days(1)).build();
        lastVisitDateStateDes.enableTimeToLive(stateTtlConfig);
        this.lastVisitDateState = getRuntimeContext().getState(lastVisitDateStateDes);
    }

    @Override
    public boolean filter(JSONObject jsonObj) throws Exception {
        //获取当前访问时间
        Long visitTime = jsonObj.getLong("visitTime");
        String udid = jsonObj.getString("udid");
        //转换为日期字符串 yyyy-MM-dd
        String currentVisitDate = TimeUtil.format(visitTime);
        //获取上次状态日期时间
        String lastVisitDate = lastVisitDateState.value();

        //用当前页面的访问时间和状态时间进行对比
        if (StringUtils.isNotBlank(lastVisitDate) && lastVisitDate.equals(currentVisitDate)) {
            System.out.println(udid + "已经在" + currentVisitDate + "时间访问过");
            return false;
        } else {
            System.out.println(udid + "在" + currentVisitDate + "时间 初次 访问");
            lastVisitDateState.update(currentVisitDate);
            return true;
        }
    }
}