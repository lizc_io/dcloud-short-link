package com.lizc.dwd;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lizc.func.VisitorMapFunction;
import com.lizc.util.DeviceUtil;
import com.lizc.util.KafkaUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;
import org.apache.flink.util.Collector;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.TreeMap;

@Slf4j
public class DwdShortLinkLogApp {

    /**
     * ODS层 kafka相关配置
     * 定义 Source Topic 与 Group
     */
    public static final String SOURCE_TOPIC = "ods_link_visit_topic";
    public static final String GROUP_ID = "dwd_short_link_group";

    /**
     * 定义 sink topic 写出到dwd层
     */
    public static final String SINK_TOPIC = "dwd_link_visit_topic";

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //设置并行度
        env.setParallelism(1);
        FlinkKafkaConsumer<String> kafkaConsumer = KafkaUtil.getKafkaConsumer(SOURCE_TOPIC, GROUP_ID);
        DataStream<String> ds = env.addSource(kafkaConsumer);

        //DataStream<String> ds = env.socketTextStream("127.0.0.1", 8888);
        ds.print();

        //数据补齐: 格式进行转换string->json、生成设备唯一标识udid和referer
        SingleOutputStreamOperator<JSONObject> jsonDS = ds.flatMap(new FlatMapFunction<String, JSONObject>() {
            @Override
            public void flatMap(String value, Collector<JSONObject> out) throws Exception {
                JSONObject jsonObject = JSON.parseObject(value);
                //生成web端设备唯一标识，放到最外层
                String udid = getDeviceId(jsonObject);
                jsonObject.put("udid", udid);

                //增加referer数据，放到最外层
                String referer = getReferer(jsonObject);
                jsonObject.put("referer",referer);
                out.collect(jsonObject);
            }
        });

        //分组：根据设备id进行分组。同一个终端设备的分到一个组里面
        KeyedStream<JSONObject, String> udidKeyStream = jsonDS.keyBy(new KeySelector<JSONObject, String>() {
            @Override
            public String getKey(JSONObject value) throws Exception {
                return value.getString("udid");
            }
        });

        //识别：richMap  open函数，做状态存储的初始化
        //标记新老访客
        SingleOutputStreamOperator<String> jsonDSWithVisitState = udidKeyStream.map(new VisitorMapFunction());
        jsonDSWithVisitState.print("新老访客ods:");

        //存储 dwd
        //将写sink到dwd层，kafka存储
        FlinkKafkaProducer<String> kafkaProducerSink = KafkaUtil.getKafkaProducer(SINK_TOPIC);
        jsonDSWithVisitState.addSink(kafkaProducerSink);

        env.execute();
    }

    /**
     * 生成设备唯一标识
     * ip + event + bizId + user-agent
     * @param jsonObject
     * @return
     */
    public static String getDeviceId(JSONObject jsonObject) {
        Map<String, String> map = new TreeMap<>();
        try {
            map.put("ip", jsonObject.getString("ip"));
            map.put("event", jsonObject.getString("event"));
            map.put("bizId", jsonObject.getString("bizId"));

            JSONObject dataJsonObj = jsonObject.getJSONObject("data");
            map.put("userAgent", dataJsonObj.getString("user-agent"));

            String deviceId = DeviceUtil.geneWebUniqueDeviceId(map);
            return deviceId;
        } catch (Exception e) {
            log.error("生产唯一deviceId异常：{}", jsonObject);
            return null;
        }

    }

    /**
     * 获取referer信息
     * @param jsonObject
     * @return
     */
    public static String getReferer(JSONObject jsonObject ){
        JSONObject dataJsonObj = jsonObject.getJSONObject("data");
        if(dataJsonObj.containsKey("referer")){
            String referer = dataJsonObj.getString("referer");
            if(StringUtils.isNotBlank(referer)){
                try {
                    URL url = new URL(referer);
                    return url.getHost();
                } catch (MalformedURLException e) {
                    log.error("提取referer失败: {}",e);
                }
            }
        }
        return "";
    }
}
