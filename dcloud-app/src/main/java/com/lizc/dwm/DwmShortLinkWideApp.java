package com.lizc.dwm;

import com.lizc.func.AsyncLocationRequestFunction;
import com.lizc.func.DeviceMapFunction;
import com.lizc.func.LocationMapFunction;
import com.lizc.model.ShortLinkWideDO;
import com.lizc.util.KafkaUtil;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer;

import java.util.concurrent.TimeUnit;

public class DwmShortLinkWideApp {

    /**
     * 定义 Source Topic
     */
    public static final String SOURCE_TOPIC = "dwd_link_visit_topic";

    /**
     * 定义消费者组
     */
    public static final String GROUP_ID = "dwm_short_link_group";


    /**
     * 定义 Sink Topic
     */
    public static final String SINK_TOPIC = "dwm_link_visit_topic";


    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //设置并行度
        env.setParallelism(1);

        //DataStream<String> ds = env.socketTextStream("127.0.0.1", 8888);

        //1、获取流
        FlinkKafkaConsumer<String> kafkaConsumer = KafkaUtil.getKafkaConsumer(SOURCE_TOPIC, GROUP_ID);
        DataStream<String> ds = env.addSource(kafkaConsumer);

        ds.print();

        //2、格式进行转换 补齐设备信息
        SingleOutputStreamOperator<ShortLinkWideDO> deviceWideDS = ds.map(new DeviceMapFunction());
        deviceWideDS.print("设备信息宽表补齐");

        //3、补齐地理位置信息
        SingleOutputStreamOperator<String> shortLinkWideDS  =
                AsyncDataStream.unorderedWait(deviceWideDS, new AsyncLocationRequestFunction(),
                        1000, TimeUnit.MILLISECONDS, 100);
        shortLinkWideDS.print("地理位置信息宽表补齐");



        //将写sink到dwm层，kafka 存储
        FlinkKafkaProducer<String> kafkaProducerSink = KafkaUtil.getKafkaProducer(SINK_TOPIC);
        shortLinkWideDS.addSink(kafkaProducerSink);

        env.execute();
    }
}
