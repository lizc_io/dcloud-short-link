package com.lizc;


import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.concurrent.*;

@Slf4j
public class FutureTest {


    @Test
    public void testFuture() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        //定义一个异步任务
        Future<String> future = executorService.submit(()->{
            //模拟耗时请求
            Thread.sleep(2000);
            return "我要去小滴课堂-学海量数据项目大课";
        });

        //一直在轮询获取结果，耗费的CPU资源
        while (true){
            if(future.isDone()) {
                System.out.println(future.get());
                break;
            }
        }
    }


    @Test
    public void testFuture2() throws ExecutionException, InterruptedException {
        //有返回值,默认使用ForkJoinPool.commonPool() 作为它的线程池执行异步代码
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() ->{
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) { }
            System.out.println(Thread.currentThread()+"执行，返回 二当家小D");
            return "二当家小D,";
        });
        System.out.println("future1返回值:" + future1.get()); //输出 二当家小D
    }

    @Test
    public void testFuture3() throws ExecutionException, InterruptedException, TimeoutException {

        //有返回值,默认使用ForkJoinPool.commonPool() 作为它的线程池执行异步代码
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() ->{
            System.out.println("执行任务一");
            return "冰冰一,";
        });

        //有返回值,当前任务正常完成以后执行，当前任务的执行的结果会作为下一任务的输入参数
        CompletableFuture<String> future2 = future1.thenApply((element) -> {
            System.out.println("入参："+element);
            System.out.println("执行任务二");
            return "冰冰二";
        });

        System.out.println("future2返回值:" + future2.get(1, TimeUnit.SECONDS));

    }


    @Test
    public void testFuture4() throws ExecutionException, InterruptedException, TimeoutException {
        //有返回值,默认使用ForkJoinPool.commonPool() 作为它的线程池执行异步代码
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() ->{
            System.out.println("执行任务一");
            return "冰冰一,";
        });
        //无返回值,当前任务正常完成以后执行,当前任务的执行结果可以作为下一任务的输入参数
        CompletableFuture<Void> future2 = future1.thenAccept((element) -> {
            System.out.println("入参："+element);
            System.out.println("执行任务二");

        });
        //System.out.println("future2返回值:" + future2.get(1, TimeUnit.SECONDS));
        System.out.println("future2返回值:" + future2.get());

    }
}
