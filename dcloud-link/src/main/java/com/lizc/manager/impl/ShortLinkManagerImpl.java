package com.lizc.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.lizc.manager.ShortLinkManager;
import com.lizc.mapper.ShortLinkMapper;
import com.lizc.model.ShortLinkDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShortLinkManagerImpl implements ShortLinkManager {

    @Autowired
    private ShortLinkMapper shortLinkMapper;

    @Override
    public int addShortLink(ShortLinkDO shortLinkDO) {
        return shortLinkMapper.insert(shortLinkDO);
    }

    @Override
    public ShortLinkDO findByShortLinkCode(String shortLinkCode) {
        ShortLinkDO shortLinkDO = shortLinkMapper.selectOne(new QueryWrapper<ShortLinkDO>().eq("code", shortLinkCode)
                .eq("del", 0));
        return shortLinkDO;
    }

    /**
     * 逻辑删除
     *
     * @param shortLinkCode
     * @param accountNo
     * @return
     */
    @Override
    public int del(String shortLinkCode, Long accountNo) {
        ShortLinkDO shortLinkDO = new ShortLinkDO();
        shortLinkDO.setDel(1);
        //这里只设置需要更新的属性就可以，属性为空不进行更新
        int rows = shortLinkMapper.update(shortLinkDO, new QueryWrapper<ShortLinkDO>()
                .eq("code", shortLinkCode).eq("account_no", accountNo));
        return rows;
    }

    @Override
    public int update(ShortLinkDO shortLinkDO) {
        //这里只设置需要更新的属性就可以，属性为空不进行更新
        int rows = shortLinkMapper.update(null, new UpdateWrapper<ShortLinkDO>()
                .eq("code", shortLinkDO.getCode()) //分片键
                .eq("del", 0)
                .eq("account_no", shortLinkDO.getAccountNo())

                .set("title", shortLinkDO.getTitle())
                .set("domain", shortLinkDO.getDomain()));
        return rows;
    }

    @Override
    public int del(ShortLinkDO shortLinkDO) {
        int rows = shortLinkMapper.update(null, new UpdateWrapper<ShortLinkDO>()
                .eq("code", shortLinkDO.getCode())
                .eq("account_no", shortLinkDO.getAccountNo())

                .set("del", 1));
        return rows;
    }
}
