package com.lizc.manager.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lizc.manager.LinkGroupManager;
import com.lizc.mapper.LinkGroupMapper;
import com.lizc.model.LinkGroupDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author lizengcai
 * @version v1.0
 * @className LinkGroupManagerImpl
 * @description TODO
 * @date 2024/3/28 23:23
 */
@Component
public class LinkGroupManagerImpl implements LinkGroupManager {

    @Autowired
    private LinkGroupMapper linkGroupMapper;

    @Override
    public int add(LinkGroupDO linkGroupDO) {
        return linkGroupMapper.insert(linkGroupDO);
    }

    @Override
    public int del(Long groupId, long accountNo) {
        return linkGroupMapper.delete(new QueryWrapper<LinkGroupDO>().eq("id", groupId).eq("account_no",accountNo));
    }

    @Override
    public LinkGroupDO detail(Long groupId, long accountNo) {
        LinkGroupDO linkGroupDO = linkGroupMapper.selectOne(new QueryWrapper<LinkGroupDO>().eq("id", groupId).eq("account_no", accountNo));
        return linkGroupDO;
    }

    @Override
    public List<LinkGroupDO> listAll(long accountNo) {
        List<LinkGroupDO> linkGroupDOList = linkGroupMapper.selectList(new QueryWrapper<LinkGroupDO>().eq("account_no", accountNo));
        return linkGroupDOList;
    }

    @Override
    public int updateById(LinkGroupDO linkGroupDO) {
        return linkGroupMapper.update(linkGroupDO,new QueryWrapper<LinkGroupDO>().eq("id",linkGroupDO.getId()).eq("account_no",linkGroupDO.getAccountNo()));
    }
}
