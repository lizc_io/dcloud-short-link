package com.lizc.manager;

import com.lizc.model.LinkGroupDO;

import java.util.List;

/**
 * @author lizengcai
 * @version v1.0
 * @className LinkGroupManager
 * @description TODO
 * @date 2024/3/28 23:23
 */
public interface LinkGroupManager {


    int add(LinkGroupDO linkGroupDO);

    int del(Long groupId, long accountNo);

    LinkGroupDO detail(Long groupId, long accountNo);

    List<LinkGroupDO> listAll(long accountNo);

    int updateById(LinkGroupDO linkGroupDO);
}
