package com.lizc.manager;

import com.lizc.enums.ShortLinkStateEnum;
import com.lizc.model.GroupCodeMappingDO;
import com.lizc.model.ShortLinkDO;

import java.util.Map;

public interface GroupCodeMappingManager {

    /**
     * 多租户商户账号查找详情
     * @param mappingId
     * @param accountNo
     * @param groupId
     * @return
     */
    GroupCodeMappingDO findByGroupIdAndMappingId(Long mappingId, Long accountNo, Long groupId);


    /**
     * 新增
     * @param groupCodeMappingDO
     * @return
     */
    int add(GroupCodeMappingDO groupCodeMappingDO);


    /**
     * 根据短链码删除
     * @param shortLinkCode
     * @param accountNo
     * @param groupId
     * @return
     */
    int del(String shortLinkCode,Long accountNo,Long groupId);


    /**
     * 分页查找
     * @param page
     * @param size
     * @param accountNo
     * @param groupId
     * @return
     */
    Map<String,Object> pageShortLinkByGroupId(Integer page, Integer size, Long accountNo, Long groupId);


    /**
     * 更新短链码状态
     * @param accountNo
     * @param groupId
     * @param shortLinkCode
     * @param shortLinkStateEnum
     * @return
     */
    int updateGroupCodeMappingState(Long accountNo, Long groupId, String shortLinkCode, ShortLinkStateEnum shortLinkStateEnum);

    GroupCodeMappingDO findByCodeAndGroupId(String shortLinkCode, Long groupId, Long accountNo);

    int update(GroupCodeMappingDO groupCodeMappingDO);

    int del(GroupCodeMappingDO groupCodeMappingDO);
}
