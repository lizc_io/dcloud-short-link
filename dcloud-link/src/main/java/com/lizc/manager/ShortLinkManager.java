package com.lizc.manager;

import com.lizc.model.ShortLinkDO;

public interface ShortLinkManager {

    /**
     * 新增域名
     *
     * @param shortLinkDO
     * @return
     */
    int addShortLink(ShortLinkDO shortLinkDO);

    /**
     * 根据短链码找内容
     *
     * @param shortLinkCode
     * @return
     */
    ShortLinkDO findByShortLinkCode(String shortLinkCode);

    /**
     * 根据短链码和accountNo删除
     *
     * @param shortLinkCode
     * @param accountNo
     * @return
     */
    int del(String shortLinkCode, Long accountNo);

    int update(ShortLinkDO shortLinkDO);

    int del(ShortLinkDO shortLinkDO);
}
