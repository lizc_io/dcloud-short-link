package com.lizc.mapper;

import com.lizc.model.LinkGroupDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-28
 */
public interface LinkGroupMapper extends BaseMapper<LinkGroupDO> {

}
