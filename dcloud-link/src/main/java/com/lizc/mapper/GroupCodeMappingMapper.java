package com.lizc.mapper;

import com.lizc.model.GroupCodeMappingDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-30
 */
public interface GroupCodeMappingMapper extends BaseMapper<GroupCodeMappingDO> {

}
