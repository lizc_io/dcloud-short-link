package com.lizc.mapper;

import com.lizc.model.DomainDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-30
 */
public interface DomainMapper extends BaseMapper<DomainDO> {

}
