package com.lizc.mapper;

import com.lizc.model.ShortLinkDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-28
 */
public interface ShortLinkMapper extends BaseMapper<ShortLinkDO> {

}
