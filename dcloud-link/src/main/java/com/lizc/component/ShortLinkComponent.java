package com.lizc.component;

import com.lizc.strategy.ShardingDBConfig;
import com.lizc.strategy.ShardingTableConfig;
import com.lizc.util.CommonUtil;
import org.springframework.stereotype.Component;

/**
 * @author lizengcai
 * @version v1.0
 * @className com.lizc.component.ShortLinkComponent
 * @description TODO
 * @date 2024/3/27 23:31
 */
@Component
public class ShortLinkComponent {

    /**
     * 62个字符
     */
    private static final String CHARS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * 生成短链码
     * @param param
     * @return
     */
    public String createShortLinkCode(String param){
        long murmurhash = CommonUtil.murmurHash32(param);
        //进制转换
        String code = encodeToBase62(murmurhash);
        //拼接库表位
        String shortLinkCode = ShardingDBConfig.getRandomDBPrefix(code) + code + ShardingTableConfig.getRandomTableSuffix(code);
        return shortLinkCode;
    }

    /**
     * 10进制转62进制
     * @param num
     * @return
     */
    private String encodeToBase62(long num){
        // StringBuffer线程安全，StringBuilder线程不安全
        StringBuffer sb = new StringBuffer();
        do{
            int i = (int)(num%62);
            sb.append(CHARS.charAt(i));
            num = num/62;
        }while (num>0);
        String value = sb.reverse().toString();
        return value;
    }
}
