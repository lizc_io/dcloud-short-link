package com.lizc.listener;


import com.lizc.enums.BizCodeEnum;
import com.lizc.enums.EventMessageType;
import com.lizc.exception.BizException;
import com.lizc.model.EventMessage;
import com.lizc.service.ShortLinkService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@RabbitListener(queuesToDeclare = { @Queue("short_link.update.mapping.queue") })
@Component
public class ShortLinkUpdateMappingMQListener {

    @Autowired
    private ShortLinkService shortLinkService;

    @RabbitHandler
    public void shortLinkHandler(EventMessage eventMessage, Message message, Channel channel) throws IOException {
        log.info("ShortLinkUpdateMappingMQListener 监听到消息, message消息内容:{}", message);
        try{
            eventMessage.setEventMessageType(EventMessageType.SHORT_LINK_UPDATE_MAPPING.name());
            shortLinkService.handleUpdateShortLink(eventMessage);
        }catch (Exception e){
            //处理业务异常，还有进行其他操作，比如记录失败原因
            log.error("消费失败:{}", eventMessage);
            throw new BizException(BizCodeEnum.MQ_CONSUME_EXCEPTION);
        }
        log.info("ShortLinkAddMappingMQListener-消费成功:{}", eventMessage);
        //手动确认消息消费成功
        //channel.basicAck(msgTag, false);
    }
}
