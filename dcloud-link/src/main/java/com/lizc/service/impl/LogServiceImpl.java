package com.lizc.service.impl;

import com.lizc.enums.LogTypeEnum;
import com.lizc.model.LogRecord;
import com.lizc.service.LogService;
import com.lizc.util.CommonUtil;
import com.lizc.util.JsonData;
import com.lizc.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Slf4j
@Service
public class LogServiceImpl implements LogService {

    private static final String TOPIC_NAME = "ods_link_visit_topic";

    private static List<String> ipList = new ArrayList<>();
    static {
        //深圳
        ipList.add("14.197.9.110");
        //广州
        ipList.add("113.68.152.139");
    }

    private static List<String> refererList = new ArrayList<>();
    static {
        refererList.add("https://taobao.com");
        refererList.add("https://douyin.com");
    }

    @Autowired
    private KafkaTemplate kafkaTemplate;

    /**
     * 记录短链码日志
     * @param request
     * @param shortLinkCode
     * @param accountNo
     * @return
     */
    @Override
    public JsonData recordShortLinkLog(HttpServletRequest request, String shortLinkCode, Long accountNo) {
        // ip、浏览器信息、短链码、账号
        //根据ip获取地理位置
        //String ip = CommonUtil.getIpAddr(request);

        Random random = new Random();
        String ip = ipList.get(random.nextInt(ipList.size())) ;
        String referer = refererList.get(random.nextInt(refererList.size()));

        //获取浏览器信息
        Map<String,String> headerMap = CommonUtil.getAllRequestHeader(request);
        Map<String,String> availableMap = new HashMap<>();
        availableMap.put("user-agent",headerMap.get("user-agent"));
        //站点来源referer
        // HTTP Referer是header的一部分，当浏览器向web服务器发送请求的时候，一般会带上Referer
        // 告诉服务器该网页是从哪个页面链接过来的，服务器因此可以获得一些信息用于处理。
        //availableMap.put("referer", headerMap.get("referer"));
        availableMap.put("referer", referer);
        //获取账号
        availableMap.put("accountNo",accountNo.toString());

        LogRecord logRecord = LogRecord.builder()
                //日志类型
                .event(LogTypeEnum.SHORT_LINK_TYPE.name())
                //日志内容
                .data(availableMap)
                //客户端ip
                .ip(ip)
                //产生时间
                .ts(CommonUtil.getCurrentTimestamp())
                //业务唯一id
                .bizId(shortLinkCode).build();
        String jsonLog = JsonUtil.obj2Json(logRecord);

        //打印控制台，方便排查
        log.info(jsonLog);

        //发送kafka
        kafkaTemplate.send(TOPIC_NAME, jsonLog);

        //存储Mysql 测试数据 TODO
        return JsonData.buildSuccess();
    }
}
