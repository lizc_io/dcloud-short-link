package com.lizc.service.impl;

import com.lizc.interceptor.LoginInterceptor;
import com.lizc.manager.DomainManager;
import com.lizc.model.DomainDO;
import com.lizc.service.DomainService;
import com.lizc.vo.DomainVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DomainServiceImpl implements DomainService {

    @Autowired
    private DomainManager domainManager;

    @Override
    public List<DomainVO> listAll() {
        Long accountNo = LoginInterceptor.threadLocal.get().getAccountNo();
        List<DomainDO> customDomainList = domainManager.listCustomDomain(accountNo);
        List<DomainDO> officialDomainList = domainManager.listOfficialDomain();
        customDomainList.addAll(officialDomainList);
        return customDomainList.stream().map(obj-> beanProcess(obj)).collect(Collectors.toList());
    }

    private DomainVO beanProcess(DomainDO domainDO){
        DomainVO domainVO = new DomainVO();
        BeanUtils.copyProperties(domainDO,domainVO);
        return domainVO;
    }
}
