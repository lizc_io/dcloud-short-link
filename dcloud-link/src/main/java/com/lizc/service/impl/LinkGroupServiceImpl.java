package com.lizc.service.impl;

import com.lizc.controller.request.LinkGroupAddRequest;
import com.lizc.controller.request.LinkGroupUpdateRequest;
import com.lizc.enums.BizCodeEnum;
import com.lizc.interceptor.LoginInterceptor;
import com.lizc.manager.LinkGroupManager;
import com.lizc.model.LinkGroupDO;
import com.lizc.model.LoginUser;
import com.lizc.service.LinkGroupService;
import com.lizc.util.IDUtil;
import com.lizc.util.JsonData;
import com.lizc.vo.LinkGroupVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-28
 */
@Service
public class LinkGroupServiceImpl implements LinkGroupService {

    @Autowired
    private LinkGroupManager linkGroupManager;

    @Override
    public JsonData add(LinkGroupAddRequest linkGroupAddRequest) {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();

        LinkGroupDO linkGroupDO = new LinkGroupDO();
        linkGroupDO.setAccountNo(loginUser.getAccountNo());
        linkGroupDO.setTitle(linkGroupAddRequest.getTitle());

        int rows = linkGroupManager.add(linkGroupDO);
        return rows == 1 ? JsonData.buildSuccess():JsonData.buildResult(BizCodeEnum.GROUP_NOT_EXIST);
    }

    @Override
    public JsonData del(Long groupId) {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        int rows = linkGroupManager.del(groupId, loginUser.getAccountNo());
        return rows == 1 ? JsonData.buildSuccess():JsonData.buildResult(BizCodeEnum.GROUP_NOT_EXIST);
    }

    @Override
    public LinkGroupVO detail(Long groupId) {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        LinkGroupDO linkGroupDO = linkGroupManager.detail(groupId, loginUser.getAccountNo());
        LinkGroupVO linkGroupVO = new LinkGroupVO();
        BeanUtils.copyProperties(linkGroupDO, linkGroupVO);
        return linkGroupVO;
    }

    @Override
    public List<LinkGroupVO> listAllGroup() {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        List<LinkGroupDO> linkGroupDOList = linkGroupManager.listAll(loginUser.getAccountNo());
        List<LinkGroupVO> groupList = linkGroupDOList.stream().map(linkGroupDO -> {
            LinkGroupVO linkGroupVO = new LinkGroupVO();
            BeanUtils.copyProperties(linkGroupDO, linkGroupVO);
            return linkGroupVO;
        }).collect(Collectors.toList());
        return groupList;
    }

    @Override
    public int updateById(LinkGroupUpdateRequest request) {
        LoginUser loginUser = LoginInterceptor.threadLocal.get();
        LinkGroupDO linkGroupDO = new LinkGroupDO();
        linkGroupDO.setId(request.getId());
        linkGroupDO.setTitle(request.getTitle());
        linkGroupDO.setAccountNo(loginUser.getAccountNo());
        int rows = linkGroupManager.updateById(linkGroupDO);
        return rows;
    }
}
