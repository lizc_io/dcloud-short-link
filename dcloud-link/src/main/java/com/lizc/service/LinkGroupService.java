package com.lizc.service;


import com.lizc.controller.request.LinkGroupAddRequest;
import com.lizc.controller.request.LinkGroupUpdateRequest;
import com.lizc.util.JsonData;
import com.lizc.vo.LinkGroupVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-28
 */
public interface LinkGroupService {

    JsonData add(LinkGroupAddRequest linkGroupAddRequest);

    JsonData del(Long groupId);

    LinkGroupVO detail(Long groupId);

    List<LinkGroupVO> listAllGroup();

    int updateById(LinkGroupUpdateRequest request);
}
