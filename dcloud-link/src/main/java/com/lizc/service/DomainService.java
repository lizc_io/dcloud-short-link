package com.lizc.service;

import com.lizc.vo.DomainVO;

import java.util.List;

public interface DomainService {

    List<DomainVO> listAll();
}
