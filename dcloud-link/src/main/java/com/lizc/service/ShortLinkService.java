package com.lizc.service;


import com.lizc.controller.request.ShortLinkAddRequest;
import com.lizc.controller.request.ShortLinkDelRequest;
import com.lizc.controller.request.ShortLinkPageRequest;
import com.lizc.controller.request.ShortLinkUpdateRequest;
import com.lizc.model.EventMessage;
import com.lizc.util.JsonData;
import com.lizc.vo.ShortLinkVO;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-28
 */
public interface ShortLinkService {

    ShortLinkVO parseShortLinkCode(String shortLinkCode);

    JsonData createShortLink(ShortLinkAddRequest request);

    Map<String, Object> pageByGroupId(ShortLinkPageRequest request);

    JsonData del(ShortLinkDelRequest request);

    JsonData update(ShortLinkUpdateRequest request);


    boolean handleAddShortLink(EventMessage eventMessage);

    boolean handleUpdateShortLink(EventMessage eventMessage);

    boolean handleDelShortLink(EventMessage eventMessage);
}
