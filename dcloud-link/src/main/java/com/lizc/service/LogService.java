package com.lizc.service;

import com.lizc.util.JsonData;

import javax.servlet.http.HttpServletRequest;

public interface LogService {

    /**
     * 记录短链码日志
     * @param request
     * @param shortLinkCode
     * @param accountNo
     * @return
     */
    JsonData recordShortLinkLog(HttpServletRequest request, String shortLinkCode, Long accountNo);
}
