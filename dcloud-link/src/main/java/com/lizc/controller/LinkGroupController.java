package com.lizc.controller;


import com.lizc.controller.request.LinkGroupAddRequest;
import com.lizc.controller.request.LinkGroupUpdateRequest;
import com.lizc.enums.BizCodeEnum;
import com.lizc.service.LinkGroupService;
import com.lizc.util.JsonData;
import com.lizc.vo.LinkGroupVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/api/group/v1")
public class LinkGroupController {


    @Autowired
    private LinkGroupService linkGroupService;

    @PostMapping("/add")
    public JsonData add(@RequestBody LinkGroupAddRequest linkGroupAddRequest){
        JsonData jsonData = linkGroupService.add(linkGroupAddRequest);
        return jsonData;
    }


    @DeleteMapping("/del/{group_id}")
    public JsonData del(@PathVariable("group_id") Long groupId){
        JsonData jsonData = linkGroupService.del(groupId);
        return jsonData;
    }

    /**
     * 根据id找详情
     * @param groupId
     * @return
     */
    @GetMapping("detail/{group_id}")
    public JsonData detail(@PathVariable("group_id") Long groupId){
        LinkGroupVO linkGroupVO = linkGroupService.detail(groupId);
        return JsonData.buildSuccess(linkGroupVO);
    }

    /**
     * 列出用户全部分组
     * @return
     */
    @GetMapping("list")
    public JsonData findUserAllLinkGroup(){
        List<LinkGroupVO> list = linkGroupService.listAllGroup();
        return JsonData.buildSuccess(list);
    }

    /**
     * 列出用户全部分组
     * @return
     */
    @PutMapping("update")
    public JsonData update(@RequestBody LinkGroupUpdateRequest request){
        int rows = linkGroupService.updateById(request);
        return rows == 1 ? JsonData.buildSuccess():JsonData.buildResult(BizCodeEnum.GROUP_OPER_FAIL);
    }
}

