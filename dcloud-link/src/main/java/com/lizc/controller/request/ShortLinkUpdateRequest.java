package com.lizc.controller.request;

import lombok.Data;

/**
 * @author lizengcai
 * @version v1.0
 * @className ShortLinkUpdateRequest
 * @description TODO
 * @date 2024/4/5 0:53
 */
@Data
public class ShortLinkUpdateRequest {

    /**
     * 组
     */
    private Long groupId;

    /**
     * groupCodeMapping映射id
     */
    private Long mappingId;

    /**
     * 短链码
     */
    private String code;

    /**
     * 短链标题
     */
    private String title;

    /**
     * 域名id
     */
    private Long domainId;

    /**
     * 域名类型
     */
    private String domainType;

}
