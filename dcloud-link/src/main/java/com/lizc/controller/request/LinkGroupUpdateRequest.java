package com.lizc.controller.request;

import lombok.Data;

/**
 * @author lizengcai
 * @version v1.0
 * @className LinkGroupUpdateRequest
 * @description TODO
 * @date 2024/3/29 0:02
 */
@Data
public class LinkGroupUpdateRequest {

    private Long id;

    private String title;
}
