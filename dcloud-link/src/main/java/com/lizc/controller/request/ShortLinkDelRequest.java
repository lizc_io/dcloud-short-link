package com.lizc.controller.request;

import lombok.Data;

/**
 * @author lizengcai
 * @version v1.0
 * @className ShortLinkDelRequest
 * @description TODO
 * @date 2024/4/5 0:53
 */
@Data
public class ShortLinkDelRequest {

    /**
     * 组
     */
    private Long groupId;

    /**
     * groupCodeMapping映射id
     */
    private Long mappingId;

    /**
     * 短链码
     */
    private String code;
}
