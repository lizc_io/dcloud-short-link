package com.lizc.controller.request;

import lombok.Data;

/**
 * @author lizengcai
 * @version v1.0
 * @className LinkGroupAddRequest
 * @description TODO
 * @date 2024/3/28 23:19
 */
@Data
public class LinkGroupAddRequest {

    /**
     * 组名
     */
    private String title;
}
