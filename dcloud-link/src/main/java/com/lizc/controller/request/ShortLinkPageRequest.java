package com.lizc.controller.request;

import lombok.Data;

/**
 * @author lizengcai
 * @version v1.0
 * @className ShortLinkPageRequest
 * @description TODO
 * @date 2024/4/5 0:28
 */
@Data
public class ShortLinkPageRequest {

    private int page;

    private int size;

    private long groupId;
}
