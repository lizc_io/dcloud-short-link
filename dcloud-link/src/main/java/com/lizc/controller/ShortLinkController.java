package com.lizc.controller;


import com.lizc.controller.request.ShortLinkAddRequest;
import com.lizc.controller.request.ShortLinkDelRequest;
import com.lizc.controller.request.ShortLinkPageRequest;
import com.lizc.controller.request.ShortLinkUpdateRequest;
import com.lizc.service.ShortLinkService;
import com.lizc.util.JsonData;
import com.lizc.vo.ShortLinkVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lizengcai
 * @since 2024-03-28
 */
@RestController
@RequestMapping("/api/link/v1")
public class ShortLinkController {

    @Autowired
    private ShortLinkService shortLinkService;

    @Value("${rpc.token}")
    private String rpcToken;

    @PostMapping("add")
    public JsonData createShortLink(@RequestBody ShortLinkAddRequest request){
        JsonData jsonData = shortLinkService.createShortLink(request);
        return jsonData;
    }


    /**
     * 分页查找短链
     *
     * @return
     */
    @RequestMapping("page")
    public JsonData pageShortLinkByGroupId(@RequestBody ShortLinkPageRequest request) {
        Map<String, Object> pageResult = shortLinkService.pageByGroupId(request);
        return JsonData.buildSuccess(pageResult);
    }

    @PostMapping("del")
    public JsonData del(@RequestBody ShortLinkDelRequest request){
        JsonData jsonData = shortLinkService.del(request);
        return jsonData;
    }

    @PostMapping("update")
    public JsonData update(@RequestBody ShortLinkUpdateRequest request){
        JsonData jsonData = shortLinkService.update(request);
        return jsonData;
    }

    /**
     * rpc调用获取短链信息
     *
     * @return
     */
    @GetMapping("check")
    public JsonData simpleDetail(@RequestParam("shortLinkCode") String shortLinkCode, HttpServletRequest request) {
        String requestToken = request.getHeader("rpc-token");
        if (rpcToken.equalsIgnoreCase(requestToken)) {
            ShortLinkVO shortLinkVO = shortLinkService.parseShortLinkCode(shortLinkCode);
            return shortLinkVO == null ? JsonData.buildError("不存在") : JsonData.buildSuccess();
        } else {
            return JsonData.buildError("非法访问");
        }

    }
}

