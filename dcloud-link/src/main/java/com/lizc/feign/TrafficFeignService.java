package com.lizc.feign;

import com.lizc.controller.request.UseTrafficRequest;
import com.lizc.util.JsonData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;

@FeignClient(name = "dcloud-account-service")
public interface TrafficFeignService {

    /**
     * 使用流量包
     * @param useTrafficRequest
     * @return
     */
    @PostMapping(value = "/api/traffic/v1/reduce", headers = {"rpc-token=${rpc.token}"})
    JsonData useTraffic(@RequestBody UseTrafficRequest useTrafficRequest);
}
