package com.lizc.strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ShardingDBConfig {

    private static final Random random = new Random();

    /**
     * 存储数据库的编号
     */
    private static final List<String> dbPrefixList = new ArrayList<>();

    //配置启用那些库的前缀
    static {
        dbPrefixList.add("0");
        dbPrefixList.add("1");
        dbPrefixList.add("a");
    }

    /**
     * 获取随机数据库的前缀
     * @return
     */
    public static String getRandomDBPrefix(){
        int index = random.nextInt(dbPrefixList.size());
        return dbPrefixList.get(index);
    }

    /**
     * 获取随机的前缀
     * @return
     */
    public static String getRandomDBPrefix(String code){
        int hashCode = code.hashCode();
        int num = Math.abs(hashCode) % dbPrefixList.size();
        return dbPrefixList.get(num);
    }
}
