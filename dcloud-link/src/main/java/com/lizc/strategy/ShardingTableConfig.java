package com.lizc.strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ShardingTableConfig {

    private static final Random random = new Random();

    private static final List<String> tableSuffixList = new ArrayList<>();

    //配置启用那些表的后缀
    static {
        tableSuffixList.add("0");
        tableSuffixList.add("1");
    }

    /**
     * 获取随机数据表的后缀
     * @return
     */
    public static String getRandomTableSuffix(){
        int index = random.nextInt(tableSuffixList.size());
        return tableSuffixList.get(index);
    }

    /**
     * 获取随机的后缀
     * @return
     */
    public static String getRandomTableSuffix(String code){
        int hashCode = code.hashCode();
        //Math.abs 返回绝对值
        int num = Math.abs(hashCode) % tableSuffixList.size();
        return tableSuffixList.get(num);
    }
}
