package com.lizc.biz;


import com.lizc.LinkApplication;
import com.lizc.manager.DomainManager;
import com.lizc.model.DomainDO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LinkApplication.class)
@Slf4j
public class DomainTest {
    
    @Autowired
    private DomainManager domainManager;
    
    @Test
    public void testDomainList(){
        List<DomainDO> domainDOList = domainManager.listOfficialDomain();
        log.info(domainDOList.toString());
    }
}
