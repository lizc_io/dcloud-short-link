package com.lizc.biz;


import com.google.common.hash.Hashing;
import com.lizc.LinkApplication;
import com.lizc.component.ShortLinkComponent;
import com.lizc.manager.ShortLinkManager;
import com.lizc.model.ShortLinkDO;
import com.lizc.util.CommonUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

/**
 * @author lizengcai
 * @version v1.0
 * @className SmsTest
 * @description TODO
 * @date 2024/3/24 0:34
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LinkApplication.class)
@Slf4j
public class ShortLinkTest {

    @Autowired
    private ShortLinkComponent shortLinkComponent;

    @Autowired
    private ShortLinkManager shortLinkManager;

    /**
     * 测试短链平台
     */
    @Test
    public void testCreateShortLink() {
        Random random = new Random();
        //for (int i = 0; i < 10; i++) {
            int num1 = random.nextInt(10);
            int num2 = random.nextInt(10000000);
            int num3 = random.nextInt(10000000);
            String originalUrl = num1 + "xdclass" + num2 + ".net" + num3;
            String shortLinkCode = shortLinkComponent.createShortLinkCode(originalUrl);
            log.info("originalUrl:" + originalUrl + ", shortLinkCode=" + shortLinkCode);
        //}
    }

    /**
     * 保存
     */
    @Test
    public void testSaveShortLink() {
        Random random = new Random();
//            for (int i = 0; i < 10; i++) {
            int num1 = random.nextInt(10);
            int num2 = random.nextInt(10000000);
            int num3 = random.nextInt(10000000);
            String originalUrl = num1 + "xdclass" + num2 + ".net" + num3;
            String shortLinkCode = shortLinkComponent.createShortLinkCode(originalUrl);
            ShortLinkDO shortLinkDO = new ShortLinkDO();
            shortLinkDO.setCode(shortLinkCode);
            shortLinkDO.setAccountNo(Long.valueOf(num3));
            shortLinkDO.setSign(originalUrl);
            shortLinkDO.setDel(0);
            shortLinkManager.addShortLink(shortLinkDO);
//        }
    }

    @Test
    public void testFind() {
        ShortLinkDO shortLinkDO = shortLinkManager.findByShortLinkCode("awzAEG1");
        log.info(shortLinkDO.toString());
    }

    @Test
    public void testGeneShortCode() {
        for(int i=0; i< 10; i++){
            String originalUrl = "www.baidu.com";
            String shortLinkCode = shortLinkComponent.createShortLinkCode(originalUrl);
            log.info("originalUrl:" + originalUrl + ", shortLinkCode=" + shortLinkCode);
        }
    }
}
